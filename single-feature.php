<?php
/**
 * Layouts: two-columns-right
 */
?>

<?php the_post(); ?>

<section class="crumbs">
    <div class="row">
        <div class="large-12 columns right">
            <?php echo $this->breadcrumbs(array('separator' => '<span class="sep"><img src="'.get_bloginfo('stylesheet_directory').'/assets/svg/arrow-breadcrumb.svg" width="10" height="10" /></span>')); ?>
        </div>      
    </div>
</section>

<section class="features">
	<div class="row">
		<div class="large-8 columns">
			<?php echo $this->partial('partials/feature/singular.phtml') ?>
			<?php if ($this->moduleActive('share')): ?>
			    <?php echo $this->shareThis() ?>
			<?php endif ?>

			<div class="singular-widgets">
				<?php echo $this->sidebarArea('content-widgets') ?>
			</div>
		</div>
		<div class="large-4 columns">
			<div class="sidebar">
            	<?php echo $this->sidebarArea('col-right') ?>
            </div>
		</div>
	</div>
</section>

<section class="home-features more">
	<div class="row">

		<div class="large-12 columns">
			<div class="singular-content padleft">
				<div class="top">
					<em><?php _e('Meer tijdsbesparende features'); ?></em>
					<a class="readmore button invert arrow right" href="<?php echo get_permalink(get_option('theme_page_features')) ?>"><?php _e('Bekijk alle features'); ?></a>
				</div>

				<?php echo $this->listFeatures( array( 'queryArgs'=>array('post__not_in'=>array(get_the_ID()), 'posts_per_page'=>8), 'template'=>'partials/feature/more/list.phtml')) ?>
			</div>	
		</div>

	</div>
</section>

<?php echo $this->partial('partials/parts/packref.phtml') ?>