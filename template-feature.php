<?php
/**
 * Template Name: Features
 * Layouts: one-column
 */
?>

<?php the_post(); ?>

<section class="crumbs">
	<div class="row">
		<div class="large-12 columns right">
			<?php echo $this->breadcrumbs(array('separator' => '<span class="sep"><img src="'.get_bloginfo('stylesheet_directory').'/assets/svg/arrow-breadcrumb.svg" width="10" height="10" /></span>')); ?>
		</div>		
	</div>
</section>

<section class="features padbot">
	<div class="row">
		<div class="large-12 columns">
			<?php echo $this->partial('partials/page/singular.phtml') ?>
            <?php echo $this->listAllFeatures() ?>
		</div>

	</div>
</section>

<?php echo $this->partial('partials/parts/packref.phtml') ?>