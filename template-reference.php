<?php
/**
 * Template Name: Referenties
 * Layouts: references
 */
?>

<?php the_post(); ?>

<section class="crumbs">
	<div class="row">
		<div class="large-12 columns right">
			<?php echo $this->breadcrumbs(array('separator' => '<span class="sep"><img src="'.get_bloginfo('stylesheet_directory').'/assets/svg/arrow-breadcrumb.svg" width="10" height="10" /></span>')); ?>
		</div>		
	</div>
</section>

<section class="references ">
	<div class="row">
		<div class="large-12 columns">
			<?php echo $this->partial('partials/page/singular.phtml') ?>
            <?php echo $this->listReferences(array('queryArgs' => array('posts_per_page' => 4, 'nopaging' => false))) ?>
		</div>

	</div>
</section>

<section class="cta">
	<div class="container">
		<div class="row">
			<img class="dagen" src="<?php echo get_bloginfo('stylesheet_directory') ?>/assets/img/30dagen.png">
			<?php echo $this->sidebarArea('cta-widgets') ?>
		</div>
		<div class="row">
			<div class="large-7 columns small-centered">
				<div class="inner">
					<a class="readmore button arrow" href="<?php echo get_permalink(get_option('theme_page_offerte')) ?>"><?php _e('Offerte aanvragen'); ?></a>
					<div class="call">
						<?php _e('of bel vrijblijvend: '); ?>
						<?php echo $this->phone(get_option('theme_company_phone')) ?>
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>

<section class="references">
	<div class="row">
		<div class="large-12 columns">
            <?php echo $this->listReferences(array('queryArgs'=>array( 'posts_per_page' => 99, 'offset' => 4, 'nopaging' => false))) ?>
		</div>

	</div>
</section>

<?php echo $this->partial('partials/parts/packref.phtml') ?>