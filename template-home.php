<?php
/**
 * Template Name: Homepage
 * Layouts: home
 */
?>

<?php the_post(); ?>

<section class="usps">
	<div class="row">
		<?php echo $this->sidebarArea('top-widgets') ?>
		<div class="clearfix"></div>
		<a class="readmore button invert arrow right" href="<?php echo get_permalink(get_option('theme_page_who')) ?>"><?php _e('Speedbooks in jouw branche'); ?></a>
	</div>
</section>

<section class="home-features">
	<div class="row">
		<div class="large-12 columns">
			<h1 class="shortborder"><?php _e('Zet uw bedrijfsdata om in praktische inzichten.'); ?></h1>
		</div>

		<div class="large-6 columns dotted-divider growgrey">
			<?php echo $this->partial('partials/page/singular.phtml') ?>
		</div>

		<div class="large-6 columns growgrey">
			<div class="singular-content padleft">
				<div class="top">
					<em><?php _e('Ontdek onze tijdsbesparende features'); ?></em>
					<a class="readmore button invert arrow right" href="<?php echo get_permalink(get_option('theme_page_features')) ?>"><?php _e('Bekijk alle features'); ?></a>
				</div>

				<?php echo $this->listFeatures(array('template'=>'partials/feature/home/list.phtml')) ?>

			</div>	
		</div>

	</div>
</section>

<section class="media">
	<!-- <div class="row"> -->
		<!-- <div class="large-12 columns"> -->
			<div class="video-holder">
				<a class="video-overlay" data-target="video-container" href="http://www.youtube.com/embed/<?php echo get_option("theme_home_videoid") ?>?autoplay=1">
					<?php if ($tagline = get_post_meta(get_the_ID(), '_tagline', true)):  ?>
						<h3><?php echo $tagline ?></h3>
					<?php endif ?>
				</a>
				<div class="video-container"></div>
			</div>
		<!-- </div> -->
	<!-- </div> -->
</section>

<section class="demo">
	<div class="row">

		<?php echo $this->sidebarArea('demo-widgets') ?>

	</div>
</section>

<section class="packref">
	<div class="row this">

		<div class="large-6 columns extra">
			<h2 class="shortborder"><?php printf( __('Meer dan %s tevreden gebruikers.'), get_option('theme_advanced_satisfied') ); ?></h2>
			<?php echo $this->randomReference() ?>
			<a class="readmore button invert arrow" href="<?php echo get_permalink(get_option('theme_page_references')) ?>"><?php _e('Bekijk meer referenties'); ?></a>
		</div>

		<div class="large-6 columns extra">
			<div class="singular-content padleft">
				<div class="top">
					<em><?php _e('Geschikt voor élk boekhoudpakket, zoals:'); ?></em>
				</div>

				<?php echo $this->listPackages(array('template'=>'partials/package/home-list.phtml')) ?>

				<a class="readmore button invert arrow right" href="<?php echo get_permalink(get_option('theme_page_packages')) ?>"><?php _e('Bekijk alle pakketten'); ?></a>
			</div>
		</div>	

	</div>
	<div class="curve-top"></div>
</section>

<section class="cta">
	<div class="container nobg">
		<div class="row">
			<img class="dagen" src="<?php echo get_bloginfo('stylesheet_directory') ?>/assets/img/30dagen.png">
			<?php echo $this->sidebarArea('cta-widgets') ?>
		</div>
		<div class="row">
			<div class="large-7 columns small-centered">
				<div class="inner">
					<a class="readmore button arrow" href="<?php echo get_permalink(get_option('theme_page_offerte')) ?>"><?php _e('Offerte aanvragen'); ?></a>
					<div class="call">
						<?php _e('of bel vrijblijvend: '); ?>
						<?php echo $this->phone(get_option('theme_company_phone')) ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
