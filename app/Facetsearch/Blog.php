<?php

namespace Theme\FacetSearch;

use Webwijs\FacetSearch\AbstractFacetSearch;

/**
 * Blog Facet Search
 *
 * Adds the blog facet search.
 *
 * @author Leo Flapper <leo@webwijs.nu>
 * @version 1.0.0
 */
class Blog extends AbstractFacetSearch
{
    /**
     * Initializes the blog Facet Search
     * @return [type] [description]
     */
    public function init()
    {
         $this->defaults = array(
            'orderby' => array( 'post_date' => 'desc'),
            'posts_per_page' => 6,
            'post_parent' => 0,
            'post_type' => 'blog'
        );
        
        $this->addFilter('paginate', 'paginate', array('default' => 1));
    }
    
}
