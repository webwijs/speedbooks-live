<?php

namespace Theme\Helper;

use Webwijs\Post;
use Webwijs\Util\Arrays;

/**
 * Helper for displaying a list of features
 */
class ListPackages
{

	/**
	 * Function to list features.
	 * Arguments can be used to filter the features or display the features in a certain template.
	 * @param  array $args array containing arguments
	 * @return string $output the html output of the retrieved features in a certain template, or null if no features are found
	 */
    public function listPackages($args = null)
    {    
        $defaults = array(
            'queryArgs' => array(
                'post_type' => 'package',
                'posts_per_page' => 6,
                'nopaging' => true,
                'orderby' => 'menu_order',
                'order' => 'asc'
            ),
            'template' => 'partials/package/list.phtml',
            'vars' => ''
        );
        $args = Arrays::addAll($defaults, (array) $args);

        $output = '';
        query_posts($args['queryArgs']);
        if (have_posts()) {
            $output = $this->view->partial($args['template'], $args['vars']);
        }
        wp_reset_query();
        
        return $output;
    }
    
}