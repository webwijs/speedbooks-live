<?php

namespace Theme\Helper;

use Webwijs\Util\Arrays;
use Webwijs\System\AbstractFileSystem;
use Webwijs\Util\Strings;

class Featured
{
    /**
     * Displays a featured header.
     *
     * @param array|null $args (optional) arguments to change what is displayed.
     * @param WP_Post|null $post (optional) the post for which to display the featured header.
     * @return string the featured header.
     */
    public function featured($args = null, $post = null)
    {
        if ($post === null) {
            $post = $GLOBALS['post'];
        }
        
        $defaults = array(
            'templates' => array(
                'gallery' => "partials/featured/gallery.phtml",
                'image'   => "partials/{$post->post_type}/featured.phtml",
                'none'   => "partials/{$post->post_type}/featured.phtml",
            ),
            'vars' => null
        );
        $args = Arrays::addAll($defaults, (array) $args);
        
        // determine header type for this post.
        $type = get_post_meta($post->ID, '_header_type', true);

        if (strlen($type) == 0) {
            $type = 'none';
        }
        
        $output = '';
        if ($type != 'none') {
            // locate featured template.
            $template = $this->locateTemplate($args['templates'][$type]);
            $output = $this->view->partial($template, array_merge((array) $args['vars'], $this->getHeaderData($post->ID, $type)));
        }
        else{
            $template = $this->locateTemplate($args['templates'][$type]);
            $output = $this->view->partial($template, array_merge((array) $args['vars'], $this->getHeaderData($post->ID, $type)));
        }
        
        return $output;
    }
    
    /**
     * Returns a fully qualified path for the given template.
     *
     * @param string $template the template whose presence will be tested.
     * @param string $default the default template which will be returned on failure.
     * @return string a fully qualified path to the given template, or the default value on failure.
     * @throws InvalidArgumentException if the first argument is not of type 'string'.
     */
    private function locateTemplate($template, $default = 'partials/featured/image.phtml')
    {
        if (!is_string($template)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string as first argument; received "%s" instead',
                __METHOD__,
                (is_object($template)) ? get_class($template) : gettype($template)
            ));
        }

        $fileSystem = AbstractFileSystem::getFileSystem();
        if (!$fileSystem->isAbsolute($template)) {
            $template = Strings::addTrailing(STYLESHEETPATH, DIRECTORY_SEPARATOR) . ltrim($template, DIRECTORY_SEPARATOR);
        }


        return (file_exists($template)) ? $template : (string) $default;
    }
    
    private function getHeaderData($postId, $type = 'image')
    {
        switch ($type) {
            case 'gallery':
                $args = array(
                    'gallery_id' => array(
                        'meta_key' => '_header_gallery_id',
                        'single'   => true,
                    ),
                );
                break;
            case 'image':
            default:
                $args = array(
                    'attachment_id' => array(
                        'meta_key' => '_header_attachment_id',
                        'single'   => true,
                    ),
                );
                break;
        }
        
        $metadata = $this->getMetaData($postId, $args);
        if (!isset($metadata['attachment_id']) || !is_numeric($metadata['attachment_id'])) {
            $metadata['attachment_id'] = get_option('theme_thumbnail_header');
        }
        return $metadata;
    }
    
    /**
     * Returns an associative array containing metadata for the given post id.
     *
     * A multidimensional array is required as second argument whose structure
     * should be as following:
     *
     * array(
     *    'link' => array(
     *        'meta_key' => '_link',
     *        'single' => true,
     *    ),
     *    'names' => array(
     *        'meta_key' => '_name',
     *        'single' => false,
     *    ),
     * );
     *
     * The example given above would return something similar to:
     *
     * array(
     *    'link' => 'http://127.0.0.1/mysite/',
     *    'names' => array(
     *        'john',
     *        'jane',
     *    ),
     * );
     *
     * @param int $postId the id of a post for which to retrieve metadata.
     * @param array $args a multidimensional array consisting of information
     *                    used to retrieve metadata for the given post.
     * @return array an array containing metadata for the given post. 
     *
     */
    private function getMetaData($postId, array $args)
    {
        $metadata = array();
        foreach ($args as $key => $args) {
            $defaults = array(
                'meta_key' => '',
                'single' => true,
            );
            $args = array_merge($defaults, (array) $args);
            
            if (!empty($args['meta_key'])) {
                $metadata[$key] = get_post_meta($postId, $args['meta_key'], (bool) $args['single']);
            }
        }
        return $metadata;
    }
}
