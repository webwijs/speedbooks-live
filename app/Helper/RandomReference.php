<?php

namespace Theme\Helper;

use Webwijs\Post;
use Webwijs\Util\Arrays;

/**
 * Helper for displaying a list of features
 */
class RandomReference
{

	/**
	 * Function to list features.
	 * Arguments can be used to filter the features or display the features in a certain template.
	 * @param  array $args array containing arguments
	 * @return string $output the html output of the retrieved features in a certain template, or null if no features are found
	 */
    public function randomReference($args = null)
    {    
        $defaults = array(
            'queryArgs' => array(
                'post_type' => 'reference',
                'posts_per_page' => 1,
                'nopaging' => false,
                'orderby' => 'rand'
            ),
            'template' => 'partials/reference/random.phtml',
            'vars' => ''
        );
        $args = Arrays::addAll($defaults, (array) $args);

        $output = '';
        query_posts($args['queryArgs']);
        if (have_posts()) {
            $output = $this->view->partial($args['template'], $args['vars']);
        }
        wp_reset_query();
        
        return $output;
    }
    
}