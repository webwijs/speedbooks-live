<?php

namespace Theme\Helper;

class Header
{
    /**
     * Displays a header for the given post.
     *
     * @param array|null $args (optional) arguments that changes the behaviour of the carousel.
     * @param WP_Post|null $post (optional) the post for which to display the header.
     * @return string a string containing the HTML elements which represent the carousel.
     */
    public function header($post = null, $size = 'default-header', $id = 'header-image')
    {
        if ($post === null) {
            $post = $GLOBALS['post'];
        }
        
        $imageUrl = '';
        if (($thumbnailId = get_option('theme_thumbnail_header', '')) !== '') {
            // use default header as fallback.
            if (($url = wp_get_attachment_image_src($thumbnailId, $size)) !== false) {
                $imageUrl = $url[0];
            }
        }
        
        return $imageUrl;
    }
}
