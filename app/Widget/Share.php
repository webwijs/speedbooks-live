<?php

namespace Theme\Widget;

use Webwijs\View;

class Share extends \WP_Widget
{
    
    
    /**
     * Share widget.
     */
    public function __construct()
    {
        $options = array('description' => 'Shares', 'classname' => 'widget-share widget-bordered');
        parent::__construct('share', 'share', $options);
        
        // create admin hooks for this widget.
        if(is_admin()) {
            add_action('admin_enqueue_scripts', array($this, 'adminScripts'));
        }
    }
    
    /**
     * The form that is displayed in wp-admin and is used to save the settings 
     * for this widget.
     *
     * @param array $instance the form values stored in the database.
     */
    public function form($instance)
    {        
        $defaults = array(
            'always_share' => null,
           
        );
        $instance = array_merge($defaults, (array) $instance);
       
        $view = new View();
       
    ?>
      
         <p>
            <label for="<?php echo $this->get_field_id('always_share'); ?>">Altijd tonen: 
                <input type="checkbox" id="<?php echo $this->get_field_id('always_share'); ?>" name="<?php echo $this->get_field_name('always_share') ?>" value="1" <?php if ( 1 == $instance['always_share'] ) echo 'checked="checked"'; ?>/>
            </label>
        </p>

    <?php
    }
    
    /**
     * Filter and normalize the form values before they are updated.
     *
     * @param array $new_instance the values entered in the form.
     * @param array $old_instance the previous form values stored in the database.
     * @return array the filtered form values that will replace the old values.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        
        $instance['always_share'] = strip_tags($new_instance['always_share']);

        return $instance;
    }
    
    /**
     * Displays the widget using values retrieved from the database.
     *
     * @param array $args an array containing (generic) arguments for all widgets.
     * @param array $instance array the values stored in the database. 
     */

    public function widget($args, $instance)
    {

        $defaults = array(
            'title'         => '',
            'always_share' => '',
        );
        $instance = array_merge($defaults, (array) $instance);
        
        $view = new View();
  
        if ($this->isAllowed($instance)) {
            // echo $args['before_widget'];
            echo $view->shareThis();
            // echo $args['after_widget'];
        }
    }

    /**
     * Determines if the share functionalty should be displayed for the given post.
     *
     * @param array $instance array the values stored in the database. 
     * @param WP_POST|null $post (optional) the post on which the share functionalty will be displayed.
     * @return bool true if the share functionalty should be displayed for the given post, false otherwise.
     */
    public function isAllowed($instance, $post = null)
    {
        if (!is_object($post)) {
            $post = $GLOBALS['post'];
        }

        $isAllowed = true;
        if (!array_key_exists('always_share', $instance) || !$instance['always_share']) {
            $isAllowed = ('1' == get_post_meta($post->ID, '_sharing_allowed', true));
        }

        return $isAllowed;
    }

    public function adminScripts($hook)
    {
        if ('widgets.php' != $hook) {
            return;
        }
        
        wp_enqueue_script('jquery-load-image-url', get_bloginfo('stylesheet_directory') . '/assets/lib/js/admin/jquery.load-image-url.js', array('jquery'), false, true);
        wp_enqueue_style('admin-lib-style', get_bloginfo('stylesheet_directory') . '/assets/lib/css/admin.css');
    }


}
