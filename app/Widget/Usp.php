<?php

namespace Theme\Widget;

use Webwijs\View;
use Webwijs\Http\Request;

class Usp extends AbstractImageWidget
{
    /**
     * Create USP widget.
     */
    public function __construct()
    {
        $options = array('description' => 'Toon een unique selling point.', 'classname' => 'widget-usp');
        parent::__construct('Theme_Widget_Usp', 'Unique selling point', $options);
        

    }
    
    /**
     * The form that is displayed in wp-admin and is used to save the settings 
     * for this widget.
     *
     * @param array $instance the form values stored in the database.
     */
    public function form($instance)
    {    
        $defaults = array(
            'attachment_id'   => '',
            'title'           => '',
            'content'         => '',
            'filter'         => '',
            'classname'       => '',
        );
        $instance = array_merge($defaults, (array) $instance);

        $images = get_posts(array(
            'post_type'      => 'attachment', 
            'post_mime_type' => 'image', 
            'post_status'    => 'inherit', 
            'posts_per_page' => -1,
        ));
        $view = new View();
        
        if (is_numeric($instance['attachment_id']) && ($url = wp_get_attachment_url($instance['attachment_id']))): ?>
        <div class="image-container">
            <img src="<?php echo esc_url($url) ?>" />
        </div>
        <?php endif ?>
        <p>
            <label for="<?php echo $this->get_field_id('attachment_id') ?>">Afbeelding:</label>
            <?php echo $view->dropdown($this->get_field_name('attachment_id'), array(
                'class' => 'widefat widget-image-dropdown-field',
                'selected' => $instance['attachment_id'],
                'options' => $this->asOptions($images),
            )); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('title') ?>">Titel:</label>
            <input class="widefat" type="text" name="<?php echo $this->get_field_name('title') ?>" value="<?php echo $instance['title'] ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('content') ?>">Content:</label>
            <textarea rows="4" class="widefat" name="<?php echo $this->get_field_name('content') ?>"><?php echo $instance['content'] ?></textarea>
        </p>
        <p>
            <input id="<?php echo $this->get_field_id('filter'); ?>" name="<?php echo $this->get_field_name('filter'); ?>" type="checkbox" <?php checked($instance['filter']) ?> /> 
            <label for="<?php echo $this->get_field_id('filter'); ?>"><?php _e('Automatically add paragraphs'); ?></label></p>
        </p>
        <p>
            <label>Css-class: <small style="font-weight: bold; float: right;">(optioneel)</small><br />
            <input class="widefat" type="text" name="<?php echo $this->get_field_name('classname') ?>" value="<?php echo $instance['classname'] ?>" />
            </label>
        </p>        
    <?php
    }
    
    /**
     * Filter and normalize the form values before they are updated.
     *
     * @param array $new_instance the values entered in the form.
     * @param array $old_instance the previous form values stored in the database.
     * @return array the filtered form values that will replace the old values.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['attachment_id'] = (isset($new_instance['attachment_id']) && is_numeric($new_instance['attachment_id'])) ? (int) $new_instance['attachment_id'] : '';
        $instance['classname'] = (is_string($new_instance['classname']) && strlen($new_instance['classname'])) ? strip_tags($new_instance['classname']) : null;
        $instance['title'] = (isset($new_instance['title'])) ? strip_tags($new_instance['title']): null;
        $instance['filter'] = isset($new_instance['filter']);
        
		if (current_user_can('unfiltered_html')) {
			$instance['content'] =  $new_instance['content'];
		} else {
			$instance['content'] = stripslashes(wp_filter_post_kses(addslashes($new_instance['content'])));
		}
		
        return $instance;
    }
    
    /**
     * Displays the widget using values retrieved from the database.
     *
     * @param array $args an array containing (generic) arguments for all widgets.
     * @param array $instance array the values stored in the database. 
     */
    public function widget($args, $instance)
    {
        $defaults = array(
            'attachment_id'   => '',
            'classname'       => '',
            'title'           => '',
            'content'         => '',
        );
        $instance = array_merge($defaults, (array) $instance);
        $view = new View();

        $instance['content'] = apply_filters('contnent', $instance['content'], $instance);
        // automatically add paragraphs.
        if (!empty($instance['filter'])) {
            $instance['content'] = wpautop($instance['content']);
        }

        // echo $args['before_widget'];
        echo $view->partial('partials/widgets/usp/widget.phtml', array_merge($args, $instance));
        // echo $args['after_widget'];
    }
}
