<?php

namespace Theme\Admin;

use Webwijs\AbstractBootstrap;
use Webwijs\Admin\Metabox\PageLayout;
use Webwijs\Admin\Metabox\Excerpt;
use Webwijs\Admin\Metabox\Multibox;
use Theme\Admin\Controller\SettingsController;
use Theme\Admin\Controller\SidebarsController;
use Theme\Admin\Metabox\Header;

class Bootstrap extends AbstractBootstrap
{    
    protected function _initSettings()
    {
        $builder = SettingsController::getBuilder();
        $builder->group('form')
                ->add('theme_form_newsletter', 'formSelect', array('label' => __('Nieuwsbriefformulier')));

        $builder->group('advanced')
                ->add('theme_advanced_flat_url', 'checkbox', array('label' => __('Platte URL\'s')))
                ->add('theme_advanced_satisfied', 'text', array('label' => __('Aantal tevreden gebruikers')));

        $builder->group('thumbnail')
                ->add('theme_thumbnail_header', 'imageSelect', array( 'label' => __('Standaard Header')));
                
        $builder->group('socialmedia')
                ->add('theme_socialmedia_twitter', 'text', array('label' => __('Twitter URL')))
                ->add('theme_socialmedia_facebook', 'text', array('label' => __('Facebook URL')))
                ->add('theme_socialmedia_linkedin', 'text', array('label' => __('LinkedIn URL')))
                ->add('theme_home_videoid', 'text', array('label' => __('Home video ID')));

        $builder->group('page')
                ->add('theme_page_404', 'postSelect', array('label' => __('404 pagina')))
                ->add('theme_page_demo', 'postSelect', array('label' => __('Demo pagina')))
                ->add('theme_page_offerte', 'postSelect', array('label' => __('Offerte pagina')))
                ->add('theme_page_who', 'postSelect', array('label' => __('Voor wie pagina')))
                ->add('theme_page_blog', 'postSelect', array('label' => __('Blog pagina')))
                ->add('theme_page_about', 'postSelect', array('label' => __('Over ons pagina')));
                
        $builder->group('company')
                ->add('theme_company_name', 'text', array('label' => __('Bedrijfsnaam')))
                ->add('theme_company_location', 'text', array('label' => __('Locatie 1 naam')))
                ->add('theme_company_address', 'text', array('type' => 'textarea', 'label' => __('Adres locatie 1')))
                ->add('theme_company_email', 'text', array('label' => __('E-mailadres (algemeen)')))
                ->add('theme_company_phone', 'text', array('label' => __('Telefoonnummer (algemeen)')))
                ->add('theme_company_helpdesk', 'text', array('label' => __('Telefoonnummer (helpdesk)')))
                ->add('theme_company_commerce_number', 'text', array('label' => __('KVK-nummer')))
                ->add('theme_company_tax_number', 'text', array('label' => __('BTW-nummer')))
                ->add('theme_company_iban_number', 'text', array('label' => __('IBAN')))
                ->add('theme_company_email_first', 'text', array('label' => __('E-mailadres (Locatie 1)')))
                ->add('theme_company_location_second', 'text', array('label' => __('Naam (Locatie 2)')))
                ->add('theme_company_address_second', 'text', array('type' => 'textarea', 'label' => __('Adres (Locatie 2)')))
                ->add('theme_company_email_second', 'text', array('label' => __('E-mailadres (Locatie 2)')))
                ->add('theme_company_phone_second', 'text', array('label' => __('Telefoonnummer (Locatie 2)')));

        new SettingsController();
    }
    
    protected function _initSidebars()
    {
        $sidebarsController = new SidebarsController();
    }
    
    protected function _initRoles()
    {
        add_filter('editable_roles', array('Theme\Admin\Filter\User', 'editableRoles'));
    }
    
    protected function _initEditor()
    {
        //add_filter('mce_external_plugins', array('Theme\Admin\Filter\TinyMCE', 'addButtonPlugin'));
        //add_filter('mce_buttons_2', array('Theme\Admin\Filter\TinyMCE', 'registerButtons'));

        add_filter('mce_buttons_2', array('Theme\Admin\Filter\TinyMCE', 'styleSelect'));
        add_filter('tiny_mce_before_init', array('Theme\Admin\Filter\TinyMCE', 'stylesDropdown'));
        
        add_action('admin_init', array('Theme\Admin\Filter\TinyMCE', 'enqueueStyles'));
        add_action('admin_head', array('Theme\Admin\Filter\TinyMCE', 'editorStyle'));
        add_filter('tiny_mce_before_init', array('Theme\Admin\Filter\TinyMCE', 'editorInit'));
    }
    
    protected function _initAdminLayout()
    {
        add_action('admin_head', array('Theme\Admin\Action\AdminLayout', 'menuLayout'));
        add_action('admin_menu', array('Theme\Admin\Action\AdminLayout', 'menuItems'));
        add_action('wp_dashboard_setup', array('Theme\Admin\Action\AdminLayout', 'removeWidgets'));
        add_action('add_meta_boxes', array('Theme\Admin\Action\AdminLayout', 'removeMetaBoxes'));
        add_action('add_meta_boxes', array('Theme\Admin\Action\YoastSeo', 'lessIntrusive'));
        add_action('add_meta_boxes', array('Theme\Admin\Action\AdminLayout', 'removeMetaBoxes'));
        add_filter('manage_posts_columns', array('Theme\Admin\Action\AdminLayout', 'listTableColumns'), 10, 2);
        add_filter('manage_pages_columns', array('Theme\Admin\Action\AdminLayout', 'listTableColumns'), 10, 2);
        foreach (get_post_types() as $post_type) {
            add_filter('get_user_option_closedpostboxes_' . $post_type, array('Theme\Admin\Action\AdminLayout', 'closedMetaBoxes'));
            add_filter('get_user_option_metaboxhidden_' . $post_type, array('Theme\Admin\Action\AdminLayout', 'hiddenMetaBoxes'));
        }
    }

    protected function _initPosts()
    {       
        PageLayout::register('post');
    }
    
    protected function _initPages()
    {       
        Multibox::register('page', array('id' => 'settings', 'title' => 'Instellingen', 'boxes' => array(
            array('class' => 'Webwijs\Admin\Metabox\Visibility'),
            array('class' => 'Webwijs\Admin\Metabox\ParentPage'),
            array('class' => 'Webwijs\Admin\Metabox\MenuOrder')
        )));

        Header::register('page');
        PageLayout::register('page');
        Excerpt::register('page');

        Multibox::register('page', array('id' => 'common', 'context' => 'normal', 'title' => 'Paginagegevens', 'boxes' => array(
            array('class' => 'Webwijs\Admin\Metabox\Textarea', 'settings' => array('id' => 'alt_title', 'title' => 'Alternatieve titel', 'options' => array('textarea_rows' => 3))),
            array('class' => 'Webwijs\Admin\Metabox\Textarea', 'settings' => array('id' => 'tagline', 'title' => 'Tagline', 'options' => array('textarea_rows' => 3))),
            // array('class' => 'Webwijs\Admin\Metabox\Textarea', 'settings' => array('id' => 'subtag', 'title' => 'Ondertitel', 'options' => array('textarea_rows' => 3)))           
        )));
    }
}
