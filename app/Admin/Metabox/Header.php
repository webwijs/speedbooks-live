<?php

namespace Theme\Admin\Metabox;

use Webwijs\Admin\AbstractMetabox;
use Webwijs\Http\Request;
use Webwijs\View;

class Header extends AbstractMetabox
{
    /**
     * A collection of valid domains.
     *
     * @var array
     */
    private $domains = array(
        'www.youtube.com',
        'youtube.com',
    );

    /**
     * settings used by the metabox.
     *
     * @var array
     */
    protected $settings = array(
        'id'       => 'header',
        'title'    => 'Header',
        'context'  => 'normal',
        'priority' => 'high',
    );
    
    /**
     * This method is run before the metabox is displayed, and can be used to register hooks for this metabox.
     *
     * @return void
     */
    public function init()
    {
        add_action('admin_enqueue_scripts', array($this, 'enqueueScripts'));
    }
    
    /**
     * Display a form or other html elements which can be used associate meta data with a particular post.
     *
     * @param WP_Post $post the post object which is currently being displayed.
     */
    public function display($post)
    {
        $type         = $this->getPostMeta($post->ID, 'type', true);
        $attachmentId = $this->getPostMeta($post->ID, 'attachment_id', true);
        $galleryId    = $this->getPostMeta($post->ID, 'gallery_id', true);
        
        $view = new View();
    ?>
        <div class="header-container advanced-layout">
            <div class="setting-fields">
                <h4>Soort header</h4>
                <fieldset>
                    <?php echo $view->dropdown($this->getName('type'), array(
                        'class'   => 'header-select widefat',
                        'options' => array(
                            'image'   => 'Afbeelding',
                            'gallery' => 'Galerij',
                            'none'    => 'Geen',
                        ),
                        'selected' => $type
                    )) ?>
                </fieldset>
            </div>      
            <div class="image-settings container <?php echo $this->matchesAny($type, array('image')) ? 'active' : '' ?>" data-type="image">
                <h4>Afbeelding</h4>
                <fieldset>
                    <div class="image-container">
                        <?php if (($url = wp_get_attachment_url($attachmentId)) !== false): ?>
                            <img src="<?php echo $url ?>" />
                        <?php endif ?>
                    </div>
                    <p>
                        <label>Selecteer een afbeelding:<br />
                            <?php echo $view->dropdownPosts(array(
                                'post_types' => 'attachment',
                                'name'       => $this->getName('attachment_id'),
                                'selected'   => $attachmentId,
                                'class'      => 'attachment-select widefat',
                            )) ?>
                        </label>
                    </p>
                </fieldset>
            </div>
            <div class="gallery-settings container <?php echo $this->matchesAny($type, array('gallery')) ? 'active' : '' ?>" data-type="gallery">
                <h4>Galerij</h4>
                <fieldset>
                    <p>
                        <label>Selecteer een galerij:<br />
                            <?php echo $view->dropdownPosts(array(
                                'post_types' => 'gallery',
                                'name'       => $this->getName('gallery_id'),
                                'selected'   => $galleryId,
                                'class'      => 'widefat',
                            )) ?>
                        </label>
                    </p>
                </fieldset>
            </div>
        </div>
        <script>
            jQuery(function($) {
                var $metabox = $('.header-container').headerMetabox();
                var $plugin = $metabox.data('header-metabox');
                
                // diplay header type.
                $metabox.on('change', '.header-select', function(e) {
                    $plugin.show('[data-type="' + $(this).val() + '"]');
                });
                
                // load selected image.                
                $metabox.on('change', '.attachment-select', function(e) {
                    $plugin.loadImage({ 
                        attachment_id: $(this).val() 
                    }, $(this).closest('.container').find('.image-container:eq(0)'));
                });
            });
        </script>
    <?php
    }
    
    /**
     * Allows the meta data entered on the admin page to be saved with a particular post.
     *
     * @param int $postId the ID of the post that the user is editing.
     */
    public function save($postId)
    {        
        $type = $this->getPostValue('type', '');
        $this->updatePostMeta($postId, 'type', $type);
        
        if ($this->matchesAny($type, array('image'))) {
            // delete post meta associated with gallery header.
            $this->deletePostMeta($postId, 'gallery_id');
            // update post meta associated with image header.
            $this->updatePostMeta($postId, 'attachment_id', $this->getPostValue('attachment_id'));
        } else if ($this->matchesAny($type, array('gallery'))) {
            // delete post meta associated with image header.
            $this->deletePostMeta($postId, 'attachment_id');
            // update post meta associated with video header.
            $this->updatePostMeta($postId, 'gallery_id', $this->getPostValue('gallery_id'));
        } else {
            // delete all post meta for this metabox.
            $this->deletePostMeta($postId, 'gallery_id');
            $this->deletePostMeta($postId, 'attachment_id');
        }
    }
    
    /**
     * Loads all the necessary scripts which allows this metabox to behave and display normal.
     *
     * @param string $hook name of the currently active page.
     */
    public function enqueueScripts($hook) 
    {
        wp_enqueue_style('admin-style', get_bloginfo('stylesheet_directory') . '/assets/lib/css/admin.css');
        wp_enqueue_script('jquery-header', get_bloginfo('stylesheet_directory') . '/assets/lib/js/admin/jquery.metabox-header.js', array('jquery'), false, true);
    }
    
    /**
     * returns true if the value matches any of the given values.
     *
     * @param mixed $value the value to be tested.
     * @param array|Traversable $values the values to match.
     * @param bool $strict if true strict comparison will be performed.
     * @return bool true if the given value matches with at least one of the values, false otherwise.
     */
    private function matchesAny($value, $values, $strict = true) 
    {
        if (!is_array($values) && !($values instanceof \Traversable)) {
            throw new \InvalidArgumentException(sprintf(
                '%s expects an array or Traversable object as argument; received "%d"',
                __METHOD__,
                (is_object($values) ? get_class($values) : gettype($values))
            ));
        }
    
        if ($values instanceof \Traversable) {
            $values = iterator_to_array($values);
        }
        
        return in_array($value, $values, (bool) $strict);
    }
}
