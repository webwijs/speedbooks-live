<?php

namespace Theme\Cpt;

use Webwijs\Cpt;

/**
 * Blog Custom Post Type
 *
 * Adds the Blog custom post type.
 *
 * @author Leo Flapper <leo@webwijs.nu>
 * @version 1.0.0
 */
class Blog extends Cpt
{
    /**
     * The post type identifier.
     * @var string
     */
    public $type = 'blog';
    
    /**
     * Initializes the post type by setting the labels and settings.
     * @return void
     */
    public function init()
    {
        $this->labels = array(
            'name' => __('Blog', 'theme'),
            'singular_name' => __('Blog', 'theme'),
            'add_new' => __('Nieuw blogbericht', 'theme'),
            'add_new_item' => __('Nieuw blogbericht', 'theme'),
            'edit_item' => __('Blogbericht bewerken', 'theme'),
            'new_item' => __('Nieuw blogbericht', 'theme'),
            'view_item' => __('Blogbericht bekijken', 'theme'),
            'search_items' => __('Blogbericht zoeken', 'theme'),
            'not_found' => __('Geen blogberichten gevonden', 'theme'),
            'not_found_in_trash' => __('Geen blogberichten gevonden', 'theme'),
            'menu_name' => __('Blog', 'theme')
        );
        
        $this->settings = array(
            'public' => true,
            'show_ui' => true,
            'supports' => array('title', 'thumbnail', 'excerpt', 'editor'),
            'capability_type' => array('blog', 'blogs'),
            'capabilities' => array(
              'edit_post'          => 'edit_blog', 
              'read_post'          => 'read_blog', 
              'delete_post'        => 'delete_blog', 
              'edit_posts'         => 'edit_blogs', 
              'edit_others_posts'  => 'edit_others_blogs', 
              'publish_posts'      => 'publish_blogs',       
              'read_private_posts' => 'read_private_blogs', 
              'create_posts'       => 'edit_blogs', 
            ),
            'map_meta_cap' => false
        );
    }
    
    /**
     * Registers the post type
     * @param  array|null $options optional options.
     * @return void
     */
    public static function register($options = null)
    {
        $cpt = new self($options);
    }
    
    /**
     * Query posts method for the custom post type.
     * @param  array $args the optional arguments list.
     * @return WP_Query the queried posts.
     */
    public static function queryPosts($args = array())
    {
        $defaults = array(
            'post_type' => 'blog',
            'orderby' => 'menu_order',
            'order' => 'ASC',
        );
        $args = array_merge($defaults, (array) $args);
        return query_posts($args);   
    }
    
}
