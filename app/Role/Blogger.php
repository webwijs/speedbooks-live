<?php

namespace Theme\Role;


class Blogger
{
    
    public function register()
    {
        $this->addRole();
    }

    private function addRole()
    {
        //remove_role('blogger');
        add_role(
            'blogger',
            __( 'Blogger' ),
            array(
                'read_blog' => true,
                'edit_blogs' => true,
                'delete_blogs' => true,
                'publish_blogs' => true,
                'upload_files' => true,
                'edit_blog' => true,
                'delete_blog' => true,
                'create_blogs' => true
            )
        );
    }

}