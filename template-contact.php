<?php
/*
 * Template Name: Contact
 * Layouts: two-columns-right
 */
?>

<?php the_post() ?>

<section class="crumbs">
    <div class="row">
        <div class="large-12 columns right">
            <?php echo $this->breadcrumbs(array('separator' => '<span class="sep"><img src="'.get_bloginfo('stylesheet_directory').'/assets/svg/arrow-breadcrumb.svg" width="10" height="10" /></span>')); ?>
        </div>      
    </div>
</section>

<section class="main">
    <div class="row">
        <div class="large-8 columns">
            <?php echo $this->partial('partials/page/singular.phtml') ?>
        </div>
        <div class="large-4 columns">
            <div class="sidebar">

                <div class="transparant bordered widget widget-contentblock">
                    <div class="entry-header">
                        <h3 class="italic"><?php echo get_option('theme_company_name') ?></h3>
                    </div>
                    <div class="entry-content">

                        <div class="phone-info">
                            <div class="inner">
                                <span class="lbl"><?php _e('Algemeen'); ?></span>
                                <?php echo $this->phone(get_option('theme_company_phone')) ?>
                            </div>
                            <div class="inner">
                                <span class="lbl"><?php _e('Helpdesk'); ?></span>
                                <?php echo $this->phone(get_option('theme_company_helpdesk')) ?>
                            </div>
                        </div>
                        <div class="contact nopad-bot">
                            <div class="email">
                                <a href="mailto:<?php echo get_option('theme_company_email') ?>"><?php echo get_option('theme_company_email') ?></a>
                            </div>
                        </div>

                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><?php _e('KVK-nummer'); ?></td>
                                <td><?php echo get_option('theme_company_commerce_number') ?></td>
                            </tr>
                            <tr>
                                <td><?php _e('BTW-nummer'); ?></td>
                                <td><?php echo get_option('theme_company_tax_number') ?></td>
                            </tr>
                            <tr>
                                <td><?php _e('IBAN'); ?></td>
                                <td><?php echo get_option('theme_company_iban_number') ?></td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="transparant bordered widget widget-contentblock">
                    <div class="entry-header">
                        <h3 class="italic"><?php echo get_option('theme_company_location') ?></h3>
                    </div>
                    <div class="entry-content">                        
                        <div class="contact">
                            <?php echo $this->phone(get_option('theme_company_phone')) ?>
                            <?php if(get_option('theme_company_email_first')): ?>
                            <div class="email">
                                <a href="mailto:<?php echo get_option('theme_company_email_first') ?>"><?php echo get_option('theme_company_email_first') ?></a>
                            </div>
                            <?php endif; ?>
                            <div class="address">
                                <?php echo str_replace(',','<br />',get_option('theme_company_address')) ?>
                            </div>
                        </div>
                        <a target="_blank" href="https://maps.google.com?daddr=<?php echo urlencode(get_option('theme_company_address', '')) ?>&zoom=10" class="readmore button invert arrow"><?php _e('Routebeschrijving'); ?></a>
                    </div>
                </div>

                <?php if(get_option('theme_company_address_second')): ?>
                <div class="transparant bordered widget widget-contentblock">
                    <div class="entry-header">
                        <h3 class="italic"><?php echo get_option('theme_company_location_second') ?></h3>
                    </div>
                    <div class="entry-content">
                        <div class="contact">
                            <?php echo $this->phone(get_option('theme_company_phone_second')) ?>
                            <?php if(get_option('theme_company_email_second')): ?>
                            <div class="email">
                                <a href="mailto:<?php echo get_option('theme_company_email_second') ?>"><?php echo get_option('theme_company_email_second') ?></a>
                            </div>
                            <?php endif; ?>
                            <div class="address">
                                <?php echo str_replace(',','<br />',get_option('theme_company_address_second')) ?>
                            </div>
                        </div>
                        <a target="_blank" href="https://maps.google.com?daddr=<?php echo urlencode(get_option('theme_company_address_second', '')) ?>&zoom=10" class="readmore button invert arrow"><?php _e('Routebeschrijving'); ?></a>
                    </div>
                </div>
                <?php endif; ?>

                <div class="transparant text-center widget widget-contentblock">
                    <div class="socialbuttons">
                        
                        <?php if(get_option('theme_socialmedia_youtube')): ?>
                            <a href="<?php echo get_option('theme_socialmedia_youtube') ?>" target="_blank"><i class="fa fa-youtube"></i></a>
                        <?php endif; ?>
                        <?php if(get_option('theme_socialmedia_linkedin')): ?>
                            <a href="<?php echo get_option('theme_socialmedia_linkedin') ?>" target="_blank"><i class="fa fa-linkedin-square"></i></a>
                        <?php endif; ?>
                        <?php if(get_option('theme_socialmedia_twitter')): ?>
                            <a href="<?php echo get_option('theme_socialmedia_twitter') ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                        <?php endif; ?>
                        <?php if(get_option('theme_socialmedia_googleplus')): ?>
                            <a href="<?php echo get_option('theme_socialmedia_googleplus') ?>" target="_blank"><i class="fa fa-google-plus"></i></a>
                        <?php endif; ?>
                        <?php if(get_option('theme_socialmedia_facebook')): ?>
                            <a href="<?php echo get_option('theme_socialmedia_facebook') ?>" target="_blank"><i class="fa fa-facebook-square"></i></a>
                        <?php endif; ?>

                        <span>
                            <?php _e('Volg ons: '); ?>
                        </span>
                    </div>
                </div>

                <?php echo $this->sidebarArea('col-right') ?>
            </div>
        </div>
    </div>
</section>

<section id="glmap" class="hide-for-small">
    <!-- <div class="infoblock">
        <h3 class="text-blue"><?php _e('Bezoekadres'); ?></h3>
        <p>
            <?php echo str_replace(',','<br />',get_option('theme_company_address')) ?>
        </p>
        <a target="_blank" href="https://maps.google.com?daddr=<?php echo urlencode(get_option('theme_company_address', '')) ?>&zoom=10" class="readmore underline"><?php _e('Route plannen op google maps'); ?></a>
    </div> -->
    <div id="map-canvas"></div>
    <input type="hidden" value="<?php echo get_option('theme_company_address') ?>" id="address">
    <input type="hidden" value="<?php bloginfo('stylesheet_directory') ?>" id="styles">
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="<?php bloginfo('stylesheet_directory') ?>/assets/js/init.maps.js"></script>
    
</section>

<?php echo $this->partial('partials/parts/packref.phtml') ?>