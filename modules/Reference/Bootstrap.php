<?php
namespace Module\Reference;

use Webwijs\AbstractBootstrap;
use Webwijs\Module\AutoloadableInterface;

use Module\Reference\Cpt\Reference as ReferenceCpt;
use Module\Reference\Admin\Bootstrap as AdminBootstrap;

/**
 * The Reference bootstrap
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap implements AutoloadableInterface
{
    /**
     * Register this module with the Standard PHP Library (SPL).
     */
    public function getAutoloaderConfig()
    {	
        return array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'Module\Reference' => __DIR__,
                ),
            ),
        );
    }

    /**
     * Initialize custom post type.
     */
    public function _initCpt()
    {
        ReferenceCpt::register(); 
    }

    /**
     * Register additional image size.
     */
    public function _initImages()
    {
        add_image_size('reference-image', 100, 220, true);
    }

    /**
     * Initialize bootstrap for admin pages.
     */
    public function _initAdmin()
    {
        $bootstrap = new AdminBootstrap();
        add_action('admin_menu', array(&$bootstrap, 'init'));    
    }
}
