<?php

namespace Module\Reference\Cpt;

use Webwijs\Cpt;

/**
 * The Reference custom post type
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Reference extends Cpt
{
    /**
     * The post type name
     *
     * @var string
     */
    public $type = 'reference';
    
    /**
     * Sets the labels and settings for the custom post type
     * @return void
     */
    public function init()
    {
        $this->labels = array(
            'name'                  => __('Referenties', 'theme'),
            'singular_name'         => __('Referentie', 'theme'),
            'add_new'               => __('Nieuwe referentie', 'theme'),
            'add_new_item'          => __('Nieuwe referentie', 'theme'),
            'edit_item'             => __('Referentie bewerken', 'theme'),
            'new_item'              => __('Nieuwe referentie', 'theme'),
            'view_item'             => __('Referentie bekijken', 'theme'),
            'search_items'          => __('Referentie zoeken', 'theme'),
            'not_found'             => __('Geen referenties gevonden', 'theme'),
            'not_found_in_trash'    => __('Geen referenties gevonden', 'theme'),
            'menu_name'             => __('Referenties', 'theme')
        );

        $this->settings = array(
            'rewrite'       => false,
            'hierarchical'  => false,
            'public'        => false,
            'show_ui'       => true,
            'supports'      => array('title', 'excerpt', 'thumbnail')
        );
    }
    
    /**
     * Registers the custom post type
     *
     * @param array $options the options for registering the custom post type
     */
    public static function register($options = null)
    {
        new self($options);
    }
    
    /**
     * Query posts of the this post type.
     * 
     * @param array $args array of arguments for retrieving the posts
     * @return WP_Query A WP_Query instance.
     */
    public static function queryPosts($args = null)
    {
        $defaults = array(
            'post_type' => $this->getType(),
            'orderby'   => 'menu_order',
            'order'     => 'ASC',
        );
        $args = array_merge( $defaults, (array) $args);
        
        return query_posts($args);   
    }

    /**
     * Returns the name of post type.
     *
     * @return string the post type name
     */
    public function getType()
    {
        return $this->type;
    }
}
