<?php

namespace Module\Reference\Admin;

use Webwijs\AbstractBootstrap;
use Webwijs\Admin\Metabox\MenuOrder;

use Module\Reference\Admin\Metabox\CustomerDetails;
use Theme\Admin\Controller\SettingsController;

/**
 * The admin bootstrap
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap
{    
    /**
     * Initialize metabox.
     */
    protected function _initMetaboxes()
    {
        MenuOrder::register('reference', array('priority' => 'default'));
        CustomerDetails::register('reference');
    }

    /**
     * Initialize settings.
     */
    protected function _initSettings()
    {
        $builder = SettingsController::getBuilder();
        $builder->group('page')
                ->add('theme_page_references', 'postSelect', array('label' => __('Referentie pagina')));
    }
}
