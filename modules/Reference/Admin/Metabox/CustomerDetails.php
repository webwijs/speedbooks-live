<?php

namespace Module\Reference\Admin\Metabox;

use Webwijs\Admin\AbstractMetabox;

/**
 * A metabox that stores details about a customer.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class CustomerDetails extends AbstractMetabox
{
    /**
     * settings used by the metabox.
     *
     * @var array
     */
    protected $settings = array(
        'id'       => 'customer',
        'title'    => 'Klantgegevens',
        'context'  => 'side',
        'priority' => 'default',
    );
    
    /**
     * Display a form which can be used associate meta data with a particular post.
     *
     * @param WP_Post $post the post object which is currently being displayed.
     */
    public function display($post)
    {
        $name    = $this->getPostMeta($post->ID, 'name', true);
        $company = $this->getPostMeta($post->ID, 'company', true);
    ?>
        <p>
            <label>Naam</label><br />
            <input class="widefat" type="text" name="<?php echo $this->getName('name') ?>" value="<?php echo esc_attr($name)?>" />
        </p>
        <p>
            <label>Bedrijfsnaam</label><br />
            <input class="widefat" type="text" name="<?php echo $this->getName('company') ?>" value="<?php echo esc_attr($company)?>" />
        </p>
    <?php
    }
    
    /**
     * Allows the meta data entered on the admin page to be saved with a particular post.
     *
     * @param int $postId the ID of the post that the user is editing.
     */
    public function save($postId)
    {
        $this->updatePostMeta($postId, 'name', $this->getPostValue('name'));
        $this->updatePostMeta($postId, 'company', $this->getPostValue('company'));
    }
}
