<?php

namespace Module\News\Helper;

use Webwijs\Application;
use Webwijs\Post;
use Webwijs\Util\Arrays;

class ListPosts
{
    /**
     * Displays a list of posts.
     *
     * If the given post or if not provided the current post is a branche only posts
     * for that branche are returned. Otherwise all posts are displayed.
     *
     * @param array|null $args (optional) array of argument to change what is displayed.
     * @param object|null $post a WP_Post object.
     * @return string html output containing events.
     */
    public function listPosts($args = null, $post = null)
    {
        if ($post === null) {
            $post = $GLOBALS['post'];
        }

        $defaults = array(
            'queryArgs' => array(
                'post_type' => 'post',
                'orderby' => 'post_date',
                'order' => 'desc',
                'post__not_in' => array($post->ID),
                'posts_per_page' => '3',
            ),
            'template' => 'partials/post/list.phtml',
            'vars' => null
        );
        $args = Arrays::addAll($defaults, (array) $args);

        // list only posts related to the branche.
        // if ($this->isBranche($post)) {
        //     $args['queryArgs']['post__in'] = $this->getRelatedPostIds($post);
        // }
        
        $output = '';
        query_posts($args['queryArgs']);
        if (have_posts()) {
            $output = $this->view->partial($args['template'], (array) $args['vars']);
        }
        wp_reset_query();
        
        return $output;
    }
        
    /**
     * Returns an array containing posts that are related with the given post.
     *
     * @param object $post the post for which to retrieve related posts.
     * @pararm string $relationKey the relation key to identify the related posts.
     * @return array zero or more posts that are related with the given post.
     * @see Post::getRelatedPostIds($key, $post)
     */
    // private function getRelatedPostIds($post, $relationKey = '_related_posts')
    // {
    //     $posts = array();
    //     if ($this->isBranche($post)) {
    //         $posts = Post::getRelatedPostIds($relationKey, $post);
    //     }
         
    //     return $posts;
    // }
       
    /**
     * Tests whether the given post is a branche.
     *
     * @param object $post the post to be tested.
     * @return bool true if the given post is a branche, false otherwise.
     */
    // private function isBranche($post)
    // {
    //     return (is_object($post) && isset($post->post_type) && $post->post_type == 'branche');
    // }
}
