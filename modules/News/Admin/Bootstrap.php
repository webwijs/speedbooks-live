<?php

namespace Module\News\Admin;

use Webwijs\AbstractBootstrap;
use Webwijs\Admin\Metabox\Multibox;
use Theme\Admin\Metabox\Header;
use Theme\Admin\Controller\SettingsController;

/**
 * The admin bootstrap
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap
{    
    /**
     * Initialize metabox.
     */
    protected function _initMetaboxes()
    {
        Multibox::register('post', array('id' => 'settings', 'title' => __('Instellingen', 'theme'), 'priority' => 'default', 'boxes' => array(
            array('class' => 'Webwijs\Admin\Metabox\Visibility'),
            array('class' => 'Webwijs\Admin\Metabox\MenuOrder')
        )));
        Header::register('post');
        Multibox::register('post', array('id' => 'common', 'context' => 'normal', 'title' => 'Paginagegevens', 'boxes' => array(
            array('class' => 'Webwijs\Admin\Metabox\Textarea', 'settings' => array('id' => 'alt_title', 'title' => 'Alternatieve titel', 'options' => array('textarea_rows' => 3))),
            array('class' => 'Webwijs\Admin\Metabox\Textarea', 'settings' => array('id' => 'tagline', 'title' => 'Tagline', 'options' => array('textarea_rows' => 3))),
            array('class' => 'Webwijs\Admin\Metabox\Textarea', 'settings' => array('id' => 'subtag', 'title' => 'Ondertitel', 'options' => array('textarea_rows' => 3)))
        )));
    }

    /**
     * Initialize settings.
     */
    protected function _initSettings()
    {
        $builder = SettingsController::getBuilder();
        $builder->group('page')
                ->add('theme_page_posts', 'postSelect', array('label' => __('Nieuws pagina')));
    }
}
