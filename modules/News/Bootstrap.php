<?php
namespace Module\News;

use Webwijs\AbstractBootstrap;
use Webwijs\Post;
use Webwijs\Module\Module;
use Webwijs\Module\ModuleAwareInterface;
use Webwijs\Module\AutoloadableInterface;

use Module\News\Admin\Bootstrap as AdminBootstrap;

class Bootstrap extends AbstractBootstrap implements ModuleAwareInterface, AutoloadableInterface
{

    public function getAutoloaderConfig()
    {	
        return array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'Module\News' => __DIR__,
                ),
            ),
        );
    }

    protected function _initPost(){
        Post::addCustomPostPageId('post', get_option('theme_page_posts'));
    }

    public function setModule(Module $module){
        $this->module = $module;
    }

    /**
     * Initialize bootstrap for admin pages.
     */
    public function _initAdmin()
    {
        $bootstrap = new AdminBootstrap();
        add_action('admin_menu', array(&$bootstrap, 'init'));    
    }

}
