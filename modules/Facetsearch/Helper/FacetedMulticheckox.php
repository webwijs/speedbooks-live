<?php

namespace Module\Facetsearch\Helper;

use Webwijs\FacetSearch\Filter\MultiFilterInterface;
use Webwijs\Util\Strings;
use Webwijs\View\Helper\FormElement;

/**
 * The FacetedMulticheckox should be considered as an adapter that allows the 'formMulticheckbox' to work
 * with classes that implement the MultiFilterInterface.
 *
 * @author Chris Harris
 * @version 0.0.1
 */
class facetedMulticheckox extends FormElement
{
    /**
     * Displays one or more checkbox for the given filter.
     *
     * @param MultiFilterInterface $filter the filter for which to display checkboxes.
     * @param array|null $attribs (optional) array of attributes to set for each checkbox.
     */
    public function facetedMulticheckox(MultiFilterInterface $filter, $attribs = array())
    {
        return $this->createCheckboxes($filter->getName(), $filter->getValue(), $attribs, $filter->getOptions());
    }
    
    /**
     * @param string $name the name for this multicheckbox.
     * @param mixed $value the value(s) for selected checkboxes.
     * @param array attributes that will be added to each checkbox.
     * @param array|string $options options for which checkboxes will be created.
     */
    private function createCheckboxes($name, $value, $attribs, $options)
    {
        if (is_string($options)) {
            $options = Strings::toArray($options);
        }
    
        $output = '<ul id="' . $name . '-input">';
        $counter = 1;
        foreach ($options as $optionValue => $optionLabel) {

            $optionLabel = (is_array($optionLabel)) ? $optionLabel['label'] : $optionLabel;
        
            $labelAttribs = array();
        
            $optionAttribs = array();
            $optionAttribs['name'] = $name . '[]';
            $optionAttribs['value'] = $optionValue;
            $optionAttribs['type'] = 'checkbox';

            if (isset($options[$optionValue]['count']) && $options[$optionValue]['count'] == 0) {
                $labelAttribs['class'] = 'disabled';
                $optionAttribs['disabled'] = 'disabled';
            }

            if((is_array($value) && in_array($optionValue, $value)) || $optionValue == $value){
                $optionAttribs['checked'] = 'checked';
            }
            $output .= '<li class="option-label">';
            $output .= '<input id="filter_'.$optionValue.'"'. $this->_renderAttribs($optionAttribs) . ' />';
            $output .= '<label for="filter_'.$optionValue.'" '. $this->_renderAttribs($labelAttribs) .'>'. $optionLabel . '</label>';
            $output .= '</li>';

            if(($counter % 3 == 0) && ($name == 'materials')){
                $output .= '<li class="splitter"></li>';
            }
            $counter++;
        }
        $output .= '</ul>';
        return $output;
    }
}
