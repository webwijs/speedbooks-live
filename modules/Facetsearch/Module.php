<?php

namespace Module\Facetsearch;

use Webwijs\Module\Module as WebwijsModule;

class Module {

    protected static $module;

    public function __construct(WebwijsModule $module = null)
    {
    	if($module){
    		self::setModule($module);	
    	}
    }

    public static function getUri()
    {
        return str_replace( strtolower(static::$module->getName()), ucfirst(static::$module->getName()), static::$module->getUri());
    }

    public static function setModule(WebwijsModule $module)
    {
    	static::$module = $module;
    }

    public static function getModule()
    {
    	return static::$module;
    }

    public static function getAssetsDirectory()
    {
        return sprintf('%s/%s', self::getUri(), 'assets');
    }

}