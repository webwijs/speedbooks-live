<?php

namespace Module\Feature\Admin;

use Webwijs\AbstractBootstrap;
use Webwijs\Admin\Metabox\Multibox;
use Webwijs\Admin\Metabox\PageLayout;
use Theme\Admin\Metabox\Header;
use Theme\Admin\Controller\SettingsController;

/**
 * The admin bootstrap
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap
{    
    /**
     * Initialize metabox.
     */
    protected function _initMetaboxes()
    {
        Multibox::register('feature', array('id' => 'settings', 'title' => __('Instellingen', 'theme'), 'priority' => 'default', 'boxes' => array(
            array('class' => 'Webwijs\Admin\Metabox\Visibility'),
            array('class' => 'Webwijs\Admin\Metabox\MenuOrder')
        )));
        Header::register('feature');
        PageLayout::register('feature');
        Multibox::register('feature', array('id' => 'common', 'context' => 'normal', 'title' => 'Paginagegevens', 'boxes' => array(
            array('class' => 'Webwijs\Admin\Metabox\Textarea', 'settings' => array('id' => 'alt_title', 'title' => 'Alternatieve titel', 'options' => array('textarea_rows' => 3))),
            array('class' => 'Webwijs\Admin\Metabox\Textarea', 'settings' => array('id' => 'tagline', 'title' => 'Tagline', 'options' => array('textarea_rows' => 3))),
            array('class' => 'Webwijs\Admin\Metabox\Textarea', 'settings' => array('id' => 'texttitle', 'title' => 'Tekst titel', 'options' => array('textarea_rows' => 3)))
        )));
    }

    /**
     * Initialize settings.
     */
    protected function _initSettings()
    {
        $builder = SettingsController::getBuilder();
        $builder->group('page')
                ->add('theme_page_features', 'postSelect', array('label' => __('Features pagina')));
    }
}
