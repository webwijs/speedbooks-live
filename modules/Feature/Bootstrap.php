<?php
namespace Module\Feature;

use Webwijs\Post;
use Webwijs\AbstractBootstrap;
use Webwijs\Module\Module;
use Webwijs\Module\ModuleAwareInterface;
use Webwijs\Module\AutoloadableInterface;

use Module\Feature\Cpt\Feature as FeatureCpt;
use Module\Feature\Admin\Bootstrap as AdminBootstrap;

/**
 * The Reference bootstrap
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap implements AutoloadableInterface
{
    /**
     * Register this module with the Standard PHP Library (SPL).
     */
    public function getAutoloaderConfig()
    {	
        return array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'Module\Feature' => __DIR__,
                ),
            ),
        );
    }
    
    /**
     * Initialize custom post type.
     */
    public function _initCpt()
    {
        Post::addCustomPostPageId('feature', get_option('theme_page_features'));
        FeatureCpt::register(); 
    }

    /**
     * Register additional image size.
     */
    public function _initImages()
    {
        add_image_size('reference-image', 100, 220, true);
    }

    /**
     * Initialize bootstrap for admin pages.
     */
    public function _initAdmin()
    {
        $bootstrap = new AdminBootstrap();
        add_action('admin_menu', array(&$bootstrap, 'init'));    
    }

}
