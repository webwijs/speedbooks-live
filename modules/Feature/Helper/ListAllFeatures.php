<?php

namespace Module\Feature\Helper;

use Webwijs\Util\Arrays;

class ListAllFeatures
{
    /**
     * Returns a list displaying all feature post types.
     *
     * @param array|null $args (optional) a collection of arguments.
     * @param WP_Post|null $post (optional) the post for 
     */
    public function listAllFeatures($args = null, $post = null)
    {
        $defaults = array(
            'queryArgs' => array(
                'post_type'      => 'feature',
                'post_status'    => 'publish',
                'orderby'        => 'menu_order',
                'order'          => 'asc',
                'posts_per_page' => -1,
            ),
            'template' => 'partials/feature/list.phtml',
            'vars' => array(),
        );
        $args = Arrays::addAll($defaults, (array) $args);
        
        $posts = get_posts($args['queryArgs']);

        $output = '';
        if (!empty($posts)) {
            $output = $this->view->partial($args['template'], array_merge((array) $args['vars'], array('posts' => $posts)));
        }
        return $output;
    }
}
