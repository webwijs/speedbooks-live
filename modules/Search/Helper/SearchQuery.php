<?php

namespace Module\Search\Helper;

use Webwijs\Http\Request;

class SearchQuery
{
    /**
     * Returns the search query from the $_GET superglobal or uses {@link get_search_query()} function as fallback.
     *
     * @param $tidy clean up the search string, this can be used to determine if the search string will be empty after tidying.
     * @return string returns the found search query.
     */
    public function searchQuery($tidy = false)
    {
        $search = (isset($_GET['search'])) ? $_GET['search'] : get_search_query();
        if (!empty($search) && $tidy) {
            preg_match_all('/".*?("|$)|((?<=[\\s",+])|^)[^\\s",+]+/', $search, $matches);
            $search = trim(implode(' ', array_map(array($this, 'sanitize'), $matches[0])));
        }
        return $search;
    }

    /**
     * return a sanitized search term by removing single and double quotes from the search term as well as any new
     * line that might be present.
     *
     * @param string $term the search term
     * @return string a sanitized search term.
     */
    private function sanitize($term)
    {
        return trim($term, "\"'\n\r ");
    }
}
