<?php

namespace Module\Search\Helper;

class SearchForm
{
    /**
     * The default form method.
     *
     * @var string
     */
    const DEFAULT_FORM_METHOD = 'post';

    /**
     * Displays a search form.
     *
     * @param array|null $args optional arguments to display the form.
     * @return string the content that the search form.
     */
    public function searchForm($args = null)
    {
        $defaults = array(
            'form' => array(
                'action' => get_option('module_page_search'),
                'method' => 'get'
            ),
            'template' => 'partials/search/form.phtml',
            'vars' => null
        );
        $args = array_merge($defaults, (array) $args);
        
        return $this->view->partial($args['template'], array_merge((array) $args['vars'], array(
            'action' => $this->getAction($args['form']['action']), 
            'method' => $this->getMethod($args['form']['method'])
        )));
    }
    
    /**
     * Returns a valid form method.
     *
     * @param string $method the method to be tested for validity.
     * @return string the given method if proven valid, otherwise the default method is returned.
     * @throws InvalidArgumentException if the given argument is not of type 'string'.
     */
    private function getMethod($method)
    {
        if (!is_string($method)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects a string argument; received "%s"',
                __METHOD__,
                (is_object($method) ? get_class($method) : gettype($method))
            ));
        }
    
        $method = strtolower($method);
        return (in_array($method, array('get', 'post'))) ? $method : DEFAULT_FORM_METHOD;
    }
    
    /**
     * Returns a sanitized url to which the form will submit it's data.
     *
     * It's asummed that a numeric value corresponds to a post id and means that this
     * method will call the {@link get_permalink($id, $leavename)} function to retrieve
     * the permalink associated with the given post.
     *
     * @param string|int $url a string representation of a url, or a post id.
     */
    private function getAction($url)
    {
        if (is_numeric($url)) {
            $url = get_permalink((int) $url);
        }
        
        return (is_string($url)) ? esc_url($url) : '';
    }
}
