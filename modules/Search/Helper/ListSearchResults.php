<?php

namespace Module\Search\Helper;

use Webwijs\Util\Arrays;

class ListSearchResults
{
    /**
     * Returns a list with posts found for the given search query.
     *
     * @param array|null $args optional array that determines what results are returned.
     * @return the posts found for the given search query.
     */
    public function listSearchResults($args = null)
    {
        $defaults = array(
            'query_args' => array(
                's' => '',
                'posts_per_page' => '5', 
                'paged' => get_query_var('paged')
            ),
            'template' => 'partials/search/results.phtml',
            'vars' => null,
        );
        $args = Arrays::addAll($defaults, (array) $args);
        
        query_posts($args['query_args']);
        
        $output = $this->view->partial('partials/search/no-results.phtml', (array) $args['vars']);
        if (have_posts()) {
            $output = $this->view->partial($args['template'], array_merge((array) $args['vars'], array('search' => $args['query_args']['s'])));
        }
        
        wp_reset_query();
        
        return $output;
    }
}
