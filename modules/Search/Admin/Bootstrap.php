<?php

namespace Module\Search\Admin;

use Theme\Admin\Controller\SettingsController;

use Webwijs\AbstractBootstrap;

/**
 * The admin bootstrap
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap
{    
    /**
     * Initialize metabox.
     */
    protected function _initSettings()
    {
        $builder = SettingsController::getBuilder();
        $builder->group('page')
                ->add('module_page_search', 'postSelect', array('label' => __('Zoekpagina')));
    }
}
