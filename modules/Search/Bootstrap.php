<?php
namespace Module\Search;

use Webwijs\AbstractBootstrap;
use Webwijs\Module\AutoloadableInterface;

/**
 * The Search bootstrap
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap implements AutoloadableInterface
{
    /**
     * Register this module with the Standard PHP Library (SPL).
     */
    public function getAutoloaderConfig()
    {	
        return array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'Module\Search' => __DIR__,
                ),
            ),
        );
    }

    /**
     * Register hooks for one or more WordPress actions.
     */
    protected function _initActions()
    {
        add_action('admin_menu', array('Module\Search\Action\Settings', 'addOptions')); 
    }
}
