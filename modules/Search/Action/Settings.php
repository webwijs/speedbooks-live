<?php

namespace Module\Search\Action;

use Theme\Admin\Controller\SettingsController;

class Settings
{
    /**
     * Add options to the {@link SettingsController} using the static form builder.
     *
     * @return void
     */
    public static function addOptions()
    {
        $builder = SettingsController::getBuilder();
        $builder->group('page')
                ->add('module_page_search', 'postSelect', array('label' => __('Zoekpagina')));
    }
}
