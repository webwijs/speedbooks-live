<?php

namespace Module\Gallery;

use Webwijs\Loader\AutoloaderFactory;
use Webwijs\Loader\ClassLoader;
use Webwijs\AbstractBootstrap;
use Webwijs\Module\Module;
use Webwijs\Module\ModuleAwareInterface;
use Webwijs\Module\AutoloadableInterface;

use Module\Gallery\Cpt\Gallery as GalleryCpt;
use Module\Gallery\Module as ModuleContainer;

use Module\Gallery\Admin\Bootstrap as AdminBootstrap;

class Bootstrap extends AbstractBootstrap implements ModuleAwareInterface, AutoloadableInterface
{
    const GALLERY_VERSION = '1.0';
    
    protected $module;

    public function getAutoloaderConfig()
    {        
        AutoloaderFactory::factory(array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'Module\Gallery' => __DIR__,
                ),
            ),
        ));
    }

    /**
     * Sets the module class
     * @param Module $module the module class
     */
    public function setModule(Module $module)
    {
        $this->module = $module;
    }

    public function getModule()
    {
    	return $this->module;
    }

    public function _initModuleContainer()
    {
    	new ModuleContainer($this->getModule());
    }

    /**
     * Add static resources to the class loader.
     */
    protected function _initResourceloaders()
    {    
        // ClassLoader::addStaticResources(array(
        //     'viewhelper'  => 'Gallery\Helper',
        // ));
    }
    
    protected function _initAjax()
    {
        add_action('wp_ajax_add-gallery-image', array('\Module\Gallery\Action\Ajax', 'addGalleryImage'));
    }

    /**
     * Initializes the admin bootstrap for the backend.
     * @return void
     */
    public function _initAdmin()
    {
        $adminBootstrap = new AdminBootstrap();
        add_action('admin_menu', array(&$adminBootstrap, 'init'));    
    }

    /**
     * Initializes the Employee custom post type
     * @return void
     */
    public function _initCpt()
    {
        GalleryCpt::register(); 
    }
}
