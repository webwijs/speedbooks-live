<?php

namespace Module\Share\Type;

use Module\Share\Dom\HtmlElement;

use Theme\Admin\Controller\Form\FormBuilderInterface;

/**
 * The TwitterType is a concrete implementation of the {@link TypeInterface} and allows a user
 * to share a web page using Twitter.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class TwitterType implements TypeInterface
{    
    /**
     * {@link inheritDoc}
     */
    public function isActive()
    {
        return (get_option('theme_share_twitter') === '1');
    }
    
    /**
     * {@link inheritDoc}
     */
    public function getElement(array $args = array())
    {
        $defaults = array(
            'url'   => '',
            'title' => '',
        );
        $args = array_merge($defaults, $args);
        
        $text = get_option('theme_share_twitter_text', '');

        $icon = new HtmlElement('i');
        $icon->addAttribute('class', 'fa fa-twitter-square');
        
        $anchor = new HtmlElement('a');
        $anchor->addAttributes(array(
            'href'        => sprintf('http://twitter.com/share?url=%1$s&amp;text=%2$s', urlencode($args['url']), urlencode($this->getMessage($text, $args))),
            'title'       => __('Share this information on Twitter'),
            'class'       => 'popup twitter',
            'data-width'  => 575,
            'data-height' => 335,
        ));
        $anchor->addChild($icon);
        
        return $anchor;  
    }
    
    /**
     * {@link inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->group('share')
                ->add('theme_share_twitter', 'checkbox', array('label' => __('Delen via Twitter')))
                ->add('theme_share_twitter_text', 'textarea', array(
                    'label'   => __('Bericht template voor twitter'),
                    'attribs' => array(
                        'cols' => 40, 
                        'rows' => 5
                    ),
                    'description' => __('Gebruik {title} voor de pagina titel en {url} voor de URL.'),
                ));
    }
    
    
    /**
     * Returns a string where possible template tags are replaced with actual values.
     *
     * @param string $message the message containing zero or more template tags.
     * @param array $replacements (optional) array containing replacements for the template tags.
     * @return string the message where all template tags are replaced with values.
     */
    private function getMessage($message = '', array $replacements = array())
    {
        $defaults = array(
            'url'   => '',
            'title' => '',
        );
        $replacements = array_intersect_key(array_merge($defaults, $replacements), $defaults);
        
        return str_replace(array('{url}', '{title}'), $replacements, $message);
    }
}
