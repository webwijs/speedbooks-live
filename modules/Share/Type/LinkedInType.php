<?php

namespace Module\Share\Type;

use Module\Share\Dom\HtmlElement;

use Theme\Admin\Controller\Form\FormBuilderInterface;

/**
 * The LinkedInType is a concrete implementation of the {@link TypeInterface} and allows a user
 * to share a web page using LinkedIn.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class LinkedInType implements TypeInterface
{
    /**
     * {@link inheritDoc}
     */
    public function isActive()
    {
        return (get_option('theme_share_linkedin') === '1');
    }
    
    /**
     * {@link inheritDoc}
     */
    public function getElement(array $args = array())
    {
        $defaults = array(
            'url' => ''
        );
        $args = array_merge($defaults, $args);
    
        $icon = new HtmlElement('i');
        $icon->addAttribute('class', 'fa fa-linkedin-square');
        
        $anchor = new HtmlElement('a');
        $anchor->addAttributes(array(
            'href'        => sprintf('http://www.linkedin.com/shareArticle?url=%1$s', urlencode($args['url'])),
            'title'       => __('Share this information on LinkedIn'),
            'class'       => 'popup linkedin',
            'data-width'  => 990,
            'data-height' => 450,
        ));
        $anchor->addChild($icon);
        
        return $anchor;  
    }
    
    /**
     * {@link inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->group('share')
                ->add('theme_share_linkedin', 'checkbox', array('label' => __('Delen via LinkedIn')));
    }
}
