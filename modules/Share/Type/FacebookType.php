<?php

namespace Module\Share\Type;

use Module\Share\Dom\HtmlElement;

use Theme\Admin\Controller\Form\FormBuilderInterface;

/**
 * The FacebookType is a concrete implementation of the {@link TypeInterface} and allows a user
 * to share a web page using Facebook.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class FacebookType implements TypeInterface
{
    /**
     * {@link inheritDoc}
     */
    public function isActive()
    {
        return (get_option('theme_share_facebook') === '1');
    }
    
    /**
     * {@link inheritDoc}
     */
    public function getElement(array $args = array())
    {
        $defaults = array(
            'url' => ''
        );
        $args = array_merge($defaults, $args);
    
        $icon = new HtmlElement('i');
        $icon->addAttribute('class', 'fa fa-facebook-square');
        
        $anchor = new HtmlElement('a');
        $anchor->addAttributes(array(
            'href'        => sprintf('//facebook.com/sharer.php?u=%1$s', urlencode($args['url'])),
            'title'       => __('Share this information on Facebook'),
            'class'       => 'popup facebook',
            'data-width'  => 575,
            'data-height' => 335,
        ));
        $anchor->addChild($icon);
        
        return $anchor;  
    }
    
    /**
     * {@link inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->group('share')
                ->add('theme_share_facebook', 'checkbox', array('label' => __('Delen via Facebook')));
    }
}
