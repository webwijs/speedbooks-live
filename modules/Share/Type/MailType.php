<?php

namespace Module\Share\Type;

use Module\Share\Dom\HtmlElement;

use Theme\Admin\Controller\Form\FormBuilderInterface;

/**
 * The FacebookType is a concrete implementation of the {@link TypeInterface} and allows a user
 * to share a web page through an e-mail.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
class MailType implements TypeInterface
{
    /**
     * {@link inheritDoc}
     */
    public function isActive()
    {
        return (get_option('theme_share_email') === '1');
    }
    
    /**
     * {@link inheritDoc}
     */
    public function getElement(array $args = array())
    {
        $defaults = array(
            'url' => ''
        );
        $args = array_merge($defaults, $args);
    
        $icon = new HtmlElement('i');
        $icon->addAttribute('class', 'fa fa-envelope-square');
        
        $anchor = new HtmlElement('a');
        $anchor->addAttributes(array(
            'href'        => 'javascript:;',
            'title'       => __('Verstuur via e-mail'),
            'class'       => 'mail',
        ));
        $anchor->addChild($icon); 
        
        return $anchor;  
    }
    
    /**
     * {@link inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder->group('share')
                ->add('theme_share_email', 'checkbox', array('label' => __('Delen via e-mail')));
    }
}
