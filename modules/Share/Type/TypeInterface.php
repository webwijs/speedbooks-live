<?php

namespace Module\Share\Type;

use Theme\Admin\Controller\Form\FormBuilderInterface;

/**
 * The TypeInterface represents a share type by which a web page can be shared.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
interface TypeInterface
{
    /**
     * Returns true if this share type is active.
     *
     * @return bool true if active, otherwise false.
     */
    public function isActive();
    
    /**
     * Returns an {@link ElementInterface} associated with this share type.
     *
     * @param array $args (optional) additional arguments to create the HTML element.
     * @return ElementInterface the HTML element.
     */
    public function getElement(array $args = array());

    /**
     * Add fields to the specified form builder.
     *
     * @param FormBuilderInterface $builder the form builder.
     */
    public function buildForm(FormBuilderInterface $builder);
}
