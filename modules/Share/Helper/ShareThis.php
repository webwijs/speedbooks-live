<?php

namespace Module\Share\Helper;

use Module\Share\Bootstrap;
use Module\Share\Type\TypeInterface;

use Webwijs\Util\Arrays;

class ShareThis
{
    /**
     * Returns a partial which displays a 'share this' link for one or more social media sites.
     * 
     * @param array|null $args optional arguments to change the share options.
     * @param object|null $post the current post to share.
     * @return string the partial containing share this links.
     * @link https://developers.facebook.com/docs/plugins/share-button/
     * @link https://dev.twitter.com/docs/tweet-button
     */
    public function shareThis($args = null, $post = null)
    {
        if (!is_object($post)) {
            $post = $GLOBALS['post'];
        }
    
        $defaults = array(
            'sharer' => array(
                'url'   => get_permalink($post->ID),
                'title' => get_the_title($post->ID),             
            ),
            'template' => 'partials/sharethis/sharethis.phtml',
            'vars' => array(),
        );
        $args = Arrays::addAll($defaults, (array) $args);

        add_action('wp_enqueue_scripts', array($this, 'enqueueScripts'));
        add_action('wp_enqueue_scripts', array($this, 'enqueueStyles'));

        $types = Bootstrap::getShareTypes();
        $types = $types->filter(function(TypeInterface $type) {
            return $type->isActive();
        });
        
        $elements = array();
        foreach ($types as $type) {
            $elements[] = $type->getElement($args['sharer']);
        }

        return $this->view->partial($args['template'], array_merge((array) $args['vars'], array('elements' => $elements)));
    }

    /**
     * Enqueue necessary JavaScript files.
     */
    public function enqueueScripts()
    {
        wp_enqueue_script('jquery.share', get_bloginfo('stylesheet_directory') . '/modules/Share/Resources/public/js/jquery.share.js', array('jquery'), false, true);
    }
    
    /**
     * Enqueue necessary stylesheets.
     */
    public function enqueueStyles()
    {
        wp_enqueue_style('share', get_bloginfo('stylesheet_directory') . '/modules/Share/Resources/public/css/share.css');
    }
}
