<?php

namespace Module\Share\Action;

use Module\Share\Bootstrap;

use Theme\Admin\Controller\SettingsController;

class Settings
{
    /**
     * Add options to the {@link SettingsController} using the static form builder.
     *
     * @return void
     */
    public static function addOptions()
    {
        $builder = SettingsController::getBuilder();
        $types   = Bootstrap::getShareTypes();
        foreach ($types as $type) {
            $type->buildForm($builder);
        }
    }
}
