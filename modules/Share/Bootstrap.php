<?php
namespace Module\Share;

use Module\Share\Type\FacebookType;
use Module\Share\Type\LinkedInType;
use Module\Share\Type\MailType;
use Module\Share\Type\TwitterType;

use Webwijs\AbstractBootstrap;
use Webwijs\Collection\ArrayList;
use Webwijs\Module\AutoloadableInterface;

/**
 * The module bootstrap
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap implements AutoloadableInterface
{
    /**
     * A collection of share types.
     * 
     * @var ListInterface
     */
    private static $types = null;

    /**
     * Register this module with the Standard PHP Library (SPL).
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'Module\Share' => __DIR__,
                ),
            ),
        );
    }
    
    /**
     * Initialize the share types.
     */
    protected function _initShareTypes()
    {
        self::$types = new ArrayList(array(
            new MailType(),
            new FacebookType(),
            new LinkedInType(),
            new TwitterType(),
        ));
    }
    
    /**
     * Register hooks for one or more WordPress actions.
     */
    protected function _initActions()
    {
        add_action('admin_menu', array('Module\Share\Action\Settings', 'addOptions')); 
    }
    
    /**
     * Returns a collection of share types.
     *  
     * @return ListInterface a collection of share types.
     */
    public static function getShareTypes()
    {
        return self::$types;
    }
}
