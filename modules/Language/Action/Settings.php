<?php

namespace Module\Language\Action;

use Theme\Admin\Controller\SettingsController;

class Settings
{
    /**
     * Add options to the {@link SettingsController} using the static form builder.
     *
     * @return void
     */
    public static function addOptions()
    {
        $builder = SettingsController::getBuilder();
        $builder->group('language')
                ->add('theme_multisite_language', 'text', array('label' => __('Label')))
                ->add('theme_multisite_language_order', 'text', array('label' => __('Volgorde'), 'attribs' => array('type' => 'number')));
    }
}
