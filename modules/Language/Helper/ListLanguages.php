<?php

namespace Module\Language\Helper;

use Webwijs\Util\Arrays;

class ListLanguages
{
    /**
     * Returns an array with all the languages for a multisite network.
     *
     * @param array|null $args (optional) argument to change what is returned.
     * @return array an array containing language options for each WordPress
     *               site installed within a multisite network.
     */
    public function listLanguages($args = null)
    {
        $defaults = array(
            'exclude' => null,
            'options' => array(
                'title' => array(
                    'option'  => 'theme_multisite_language',
                    'default' => ''
                ),
                'order' => array(
                    'option'  => 'theme_multisite_language_order',
                    'default' => 999
                )
            ),
            'template' => 'partials/languages/list.phtml',
            'vars'     => array(),
        );
        $args = Arrays::addAll($defaults, (array) $args);

        $langs = wp_cache_get('multisite_langs');
        if (false === $langs) {
            global $wpdb;
            $blogs = $wpdb->get_results("SELECT blog_id FROM {$wpdb->blogs}");
            if(is_array($blogs)) {
                $langs = array();
                foreach($blogs as $blog) {
                    // ignore multisite from the exclude list.
                    if (is_array($args['exclude']) && in_array($blog->blog_id, $args['exclude'])) {
                        continue;
                    }

                    $language = array(
                        'blog_id'    => $blog->blog_id,
                        'url'        => get_blogaddress_by_id($blog->blog_id),
                        'is_active'  => $this->blogActive($blog->blog_id),
                    );
                    $blogOptions = $this->getBlogOptions($blog->blog_id, $args['options']);

                    $langs[] = (object) array_merge($blogOptions, $language);
                }
                
                // sort languages by (multisite) order.
                usort($langs, array($this, 'compare'));
            }
            wp_cache_set('multisite_langs', $langs);
        }
        
        return $langs;
    }

    /**
     * Returns 'true' if the current blog is active.
     * 
     * @param int $blogId the blog id who will be compared with the currently active blog.
     * @return boolean returns true, if and only if the given blog id matches with the currently active blog.
     */
    private function blogActive($blogId)
    {
        $currentBlogId = get_current_blog_id();
        return (is_numeric($blogId) && $blogId == $currentBlogId);
    }
    
    /**
     * Returns an associative array of options for the given blog.
     *
     * The $options parameter should resemble the following structure:
     * 
     * array(
     *     'title' => array(
     *         'option'  => 'theme_multisite_language',
     *         'default' => 'nl'
     *     ),
     *     'order' => array(
     *         'option'  => 'theme_multisite_language_order',
     *         'default' => 10
     *     )
     * )
     *
     * @param int $blogId the blog id whose options to retrieve.
     * @param array|Traversable $options a multidimensional array describing which options to return.
     * @return array (associative) array containing the value of each option.
     */
    private function getBlogOptions($blogId, $options)
    {
        if (!is_array($options) && !($options instanceof \Traversable)) {
            throw new \InvalidArgumentException(sprintf(
                '%s: expects an array or instance of the Traversable; received "%s"',
                __METHOD__,
                (is_object($options) ? get_class($options) : gettype($options))
            ));
        }
        
        // copy iterator to array.
        if ($options instanceof \Traversable) {
            $options = iterator_to_array($options);
        }
        
        $retval = array();
        foreach ($options as $key => $option) {
            $default = (isset($option['default'])) ? $option['default'] : null;
            if (isset($option['option']) && is_string($option['option'])) {
                $retval[$key] = get_blog_option($blogId, $option['option'], $default);
            }
        }
        
        return $retval;
    }
    
    /**
     * Sort array by values using a user-defined comparison function
     *
     * PHP uses the quicksort (with in-place partitioning) which is an unstable sort and is somewhat complex, 
     * but in practice it's one of the fastest sorting algorithms. Because the sorting algorithm used by PHP
     * is unstable one should remember that the order is undefined if two values are considered equal.
     * 
     * @param mixed $first the language to compare.
     * @param mixed $second the language to compare with.
     * @return bool a negative integer, zero, or a positive integer as the first language is less than, equal to, 
     *              or greater than the second.
     * @link http://php.net/manual/en/function.usort.php
     */
    public function compare($first, $second)
    {
        // active language always first.
        if (isset($first->is_active) && $first) {
            return -1;
        }
    
        if (isset($first->order, $second->order)) {
            if ($first->order == $second->order) {
                return 0;
            }
            return ($first->order < $second->order) ? -1 : 1;
        }
        return 0;
    }
}
