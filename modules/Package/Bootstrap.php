<?php
namespace Module\Package;

use Webwijs\Post;
use Webwijs\AbstractBootstrap;
use Webwijs\Module\AutoloadableInterface;

use Module\Package\Cpt\Package as PackageCpt;
use Module\Package\Admin\Bootstrap as AdminBootstrap;

/**
 * The Package bootstrap
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap implements AutoloadableInterface
{
    /**
     * Register this module with the Standard PHP Library (SPL).
     */
    public function getAutoloaderConfig()
    {	
        return array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    'Module\Package' => __DIR__,
                ),
            ),
        );
    }

    /**
     * Initialize custom post type.
     */
    public function _initCpt()
    {
        Post::addCustomPostPageId('package', get_option('theme_page_packages'));
        PackageCpt::register(); 
    }

    /**
     * Register additional image size.
     */
    public function _initImages()
    {
        add_image_size('package-image', 180, 60, true);
    }

    /**
     * Initialize bootstrap for admin pages.
     */
    public function _initAdmin()
    {
        $bootstrap = new AdminBootstrap();
        add_action('admin_menu', array(&$bootstrap, 'init'));    
    }

}
