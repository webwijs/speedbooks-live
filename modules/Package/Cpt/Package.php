<?php

namespace Module\Package\Cpt;

use Webwijs\Cpt;

/**
 * The Package custom post type
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Package extends Cpt
{
    /**
     * The post type name
     *
     * @var string
     */
    public $type = 'package';
    
    /**
     * Sets the labels and settings for the custom post type
     * @return void
     */
    public function init()
    {
        $this->labels = array(
            'name'                  => __('Pakketten', 'theme'),
            'singular_name'         => __('Pakket', 'theme'),
            'add_new'               => __('Nieuw pakket', 'theme'),
            'add_new_item'          => __('Nieuw pakket', 'theme'),
            'edit_item'             => __('Pakket bewerken', 'theme'),
            'new_item'              => __('Nieuw pakket', 'theme'),
            'view_item'             => __('Pakket bekijken', 'theme'),
            'search_items'          => __('Pakket zoeken', 'theme'),
            'not_found'             => __('Geen pakketten gevonden', 'theme'),
            'not_found_in_trash'    => __('Geen pakketten gevonden', 'theme'),
            'menu_name'             => __('Pakketten', 'theme')
        );

        $this->settings = array(
            'rewrite'       => false,
            'hierarchical'  => false,
            'public'        => true,
            'show_ui'       => true,
            'supports'      => array('title', 'editor', 'excerpt', 'thumbnail')
        );
    }
    
    /**
     * Registers the custom post type
     *
     * @param array $options the options for registering the custom post type
     */
    public static function register($options = null)
    {
        new self($options);
    }
    
    /**
     * Query posts of the this post type.
     * 
     * @param array $args array of arguments for retrieving the posts
     * @return WP_Query A WP_Query instance.
     */
    public static function queryPosts($args = null)
    {
        $defaults = array(
            'post_type' => $this->getType(),
            'orderby'   => 'menu_order',
            'order'     => 'ASC',
        );
        $args = array_merge( $defaults, (array) $args);
        
        return query_posts($args);   
    }

    /**
     * Returns the name of post type.
     *
     * @return string the post type name
     */
    public function getType()
    {
        return $this->type;
    }
}
