<?php

namespace Module\Package\Admin;

use Webwijs\AbstractBootstrap;
use Webwijs\Admin\Metabox\Multibox;
use Theme\Admin\Controller\SettingsController;

/**
 * The admin bootstrap
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class Bootstrap extends AbstractBootstrap
{    
    /**
     * Initialize metabox.
     */
    protected function _initMetaboxes()
    {
        Multibox::register('package', array('id' => 'settings', 'title' => __('Instellingen', 'theme'), 'priority' => 'default', 'boxes' => array(
            array('class' => 'Webwijs\Admin\Metabox\Visibility'),
            array('class' => 'Webwijs\Admin\Metabox\MenuOrder')
        )));
    }

    /**
     * Initialize settings.
     */
    protected function _initSettings()
    {
        $builder = SettingsController::getBuilder();
        $builder->group('page')
                ->add('theme_page_packages', 'postSelect', array('label' => __('Pakketten pagina')));
    }
}
