<?php
/*
 * Layouts: two-columns-right
 */
?>

<?php the_post() ?>

<section class="crumbs">
    <div class="row">
        <div class="large-12 columns right">
            <?php echo $this->breadcrumbs(array('separator' => '<span class="sep"><img src="'.get_bloginfo('stylesheet_directory').'/assets/svg/arrow-breadcrumb.svg" width="10" height="10" /></span>')); ?>
        </div>      
    </div>
</section>

<section class="main">
    <div class="row">
        <div class="large-8 columns">
            <?php echo $this->partial('partials/page/singular.phtml') ?>
            <div class="singular-widgets">
                <?php echo $this->sidebarArea('content-widgets') ?>
            </div>
        </div>
        <div class="large-4 columns">
        	<div class="sidebar normal">
            	<?php echo $this->sidebarArea('col-right') ?>
            </div>
        </div>
    </div>
</section>

<?php echo $this->partial('partials/parts/packref.phtml') ?>