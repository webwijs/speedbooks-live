<?php

namespace Webwijs\Form\Element;

use Webwijs\Form\Element;

class Multicheckbox extends Element
{    
    var $is_array = true;
    public $helper = 'multicheckbox';
}
