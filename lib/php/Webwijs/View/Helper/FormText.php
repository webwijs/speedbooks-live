<?php

namespace Webwijs\View\Helper;

class FormText extends FormElement
{
    public function formText($name, $value, $attribs = array(), $options = array())
    {
        $attribs['id']    = (isset($attribs['id'])) ? $attribs['id'] : "{$name}-input";
        $attribs['name']  = $name;
        $attribs['type']  = (isset($attribs['type'])) ? $attribs['type'] : 'text';
        $attribs['value'] = $this->escape($value);
        return '<input' . $this->_renderAttribs($attribs) . '/>';
    }
}
