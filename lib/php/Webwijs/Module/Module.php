<?php

namespace Webwijs\Module;

use SplFileInfo;
use Webwijs\Loader\ClassLoader;
use Webwijs\Module\Config\Config;
use Webwijs\Loader\AutoloaderFactory;
use Webwijs\View\Directories as ViewDirectories;
use Webwijs\File\FileFinder;

/**
 * Module
 *
 * Sets the module data and bootstraps the module
 *
 * @author Leo Flapper <leo@webwijs.nu>
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */ 
class Module
{

	/**
	 * The name of the module
	 * @var string $name the name of the module
	 */
	public $name;

	/**
	 * The full path of the module
	 * @var  string $path the full path of the module
	 */
	public $path;

	/**
	 * The full uri of the module
	 * @var  string $path the full uri of the module
	 */
	public $uri;

	/**
	 * The module templates
	 * @var array $templates array containing module templates paths
	 */
	public $templates = array();

	/**
	 * The module single templates
	 * @var array $singleTemplates array containing module single templates paths
	 */
	public $singleTemplates = array();

	/**
	 * The configuration data of the module
	 * @var Config $config the configuration data
	 */
	public $config;


	/**
	 * If the module is active
	 * @var boolean $active true if active, false if not active
	 */
	public $active;

	/**
	 * The namespace of the module
	 * @var string $namespace the namespace of the module
	 */
	public $namespace;

	/**
	 * Sets the module data, adds static resources and bootstraps the module
	 * @param string $name   the name of the module
	 * @param string $path   the full path of the module
	 * @param string $uri    the full uri of the module
	 * @param Config $config the configuration data
	 */
	public function __construct($name, $path, $uri, Config $config)
	{
		$this->setName($name);
		$this->setPath($path);
		$this->setConfig($config);
		$this->setUri($uri);
		$this->setActive($config->isActive());
		$this->setTemplates();
		$this->setNamespace('\Module\\'.$this->getName());
		$this->addStaticResources();
		$this->bootstrap();
	}

	/**
	 * Adds static resources to the application for further use in the application
	 */
	private function addStaticResources()
	{
		ClassLoader::addStaticResources(array(
            'viewhelper' => $this->getNamespace().'\Helper'
        ));

		ViewDirectories::addStaticDirectory($this->getPath());
	}

	/**
	 * Bootstraps the module if the module is active
	 */
	private function bootstrap()
	{
		if($this->isActive() && $this->getPath() && $this->getName()){
			$bootstrapPath = $this->getPath().'/Bootstrap.php';
			if(file_exists($bootstrapPath)){
				include_once($bootstrapPath);
				$class = $this->getNamespace().'\Bootstrap';
				
				$bootstrap = new $class();
				
				if($bootstrap instanceof ModuleAwareInterface) { 
					$bootstrap->setModule($this);
				}

				if($bootstrap instanceof AutoloadableInterface) {
					AutoloaderFactory::factory($bootstrap->getAutoloaderConfig());
				}

				$bootstrap->init();

			} else {
				throw new \Exception(sprintf('Bootstrap not found for module %s at path %s', $this->getName(), $this->getPath()));
			}
		}
	}

	/**
	 * Returns the namespace of the module
	 * @return string $namespace the namespace of the module
	 */
	public function getNamespace()
	{
		return $this->namespace;
	}

	/**
	 * Sets the namespace of the module
	 * @param string $namespace the namespace of the module
	 */
	private function setNamespace($namespace)
	{	
		if (!is_string($namespace)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects an string argument; received "%s"',
	            __METHOD__,
	            (is_object($namespace) ? get_class($namespace) : gettype($namespace))
	        ));
	    }

		$this->namespace = $namespace;
	}

	/**
	 * Returns the name of the module
	 * @return string the name of the module
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Sets the name of the module
	 * @param string $name the name of the module
	 */
	private function setName($name)
	{
		if (!is_string($name)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects an string argument; received "%s"',
	            __METHOD__,
	            (is_object($name) ? get_class($name) : gettype($name))
	        ));
	    }

		$this->name = $name;
	}

	/**
	 * Returns the path of the module
	 * @return string $path the path of the module
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * Sets the path of the module
	 * @param string $path the path of the module
	 */
	private function setPath($path)
	{	
		if (!is_string($path)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects an string argument; received "%s"',
	            __METHOD__,
	            (is_object($path) ? get_class($path) : gettype($path))
	        ));
	    }

		$this->path = $path;
	}

	/**
	 * Returns the URI of the module
	 * @return string the uri of the module
	 */
	public function getUri()
	{
		// return $this->uri;
		return str_replace(strtolower($this->name), ucfirst($this->name), $this->uri);
	}

	/**
	 * Sets the URI of the module
	 * @param string the uri of the module
	 */
	private function setUri($uri)
	{	
		if (!is_string($uri)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects an string argument; received "%s"',
	            __METHOD__,
	            (is_object($uri) ? get_class($uri) : gettype($uri))
	        ));
	    }

		$this->uri = $uri;
	}

	public function getTemplates()
	{
		return $this->templates;
	}

	public function getSingleTemplates()
	{
		return $this->singleTemplates;
	}

	private function setTemplates()
	{
		$fileFinder = new FileFinder();
		$fileFinder->addPath($this->getPath());
		
		foreach($fileFinder as $info){
			if(substr($info->getBasename('.php'), 0, 9) === 'template-'){
				$this->setTemplate($info);
			} elseif(substr($info->getBasename('.php'), 0, 7) === 'single-'){
				$this->setSingleTemplate($info);
			}
		}
	}

	private function setTemplate(SplFileInfo $template)
	{
		$this->templates[] = $template;
	}

	private function setSingleTemplate(SplFileInfo $singleTemplate)
	{
		$this->singleTemplates[] = $singleTemplate;
	}

	/**
	 * Returns the module config data
	 * @param Config $config the module config data
	 */
	private function getConfig()
	{
		return $this->config;
	}

	/**
	 * Sets the module config data
	 * @param Config $config the module config data
	 */
	private function setConfig(Config $config)
	{
		$this->config = $config;
	}

	public function isActive()
	{
		return $this->active;
	}

	/**
	 * Sets if the module is active or not
	 * @param boolean $active true if active, false if not active
	 */
	private function setActive($active){
		if (!is_bool($active)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects a boolean argument; received "%s"',
	            __METHOD__,
	            (is_object($active) ? get_class($active) : gettype($active))
	        ));
	    }

	    $this->active = $active;
	}

}
