<?php

namespace Webwijs\Module;

use ReflectionClass;
use Memcached;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\MemcachedCache;
use Webwijs\File\FileFinder;
use Webwijs\Module\CachedModuleLoader;
use Webwijs\Module\ModuleLoader;

/**
 * 
 * Module Facade
 *
 * Returns the modules by initiating the module loader
 * 
 * @author Leo Flapper <leo@webwijs.nu>
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class ModuleFacade
{
	/**
	 * Default directory containing modules
	 * @var string $defaultDirectory default directory containing modules
	 */
	private $defaultDirectory = '/modules';

	/**
	 * The cache key to use for caching the routes
	 */
	const CACHE_KEY = 'modules';

	/**
	 * The directory where the routes are located
	 * @var string
	 */
	protected $directory = 'Controller';

	/**
	 * The cachedriver to use
	 * @var object
	 */
	protected $cacheDriver;

	/**
	 * Contains the loaded modules
	 * @var array
	 */
	protected $modules = array();

	/**
	 * Returns the loaded modules
	 * @return array $modules the loaded modules, or empty if no modules
	 */
	public function getModules()
	{
		if(!$this->modules){
			$this->setModules();
		}

		return $this->modules;
	}

	/**
	 * Sets the modules by initiating the module loaders to retrieve the modules
	 */
	private function setModules()
	{
		$fileFinder = new FileFinder();
		$fileFinder->setFilter(new ModuleFilter());
		$fileFinder->setMaxDepth(2);
		$fileFinder->addPath($this->getModulesDirectory());

		$loader = new CachedModuleLoader(new ModuleLoader(), $this->getCacheDriver(), self::CACHE_KEY);
		$this->modules = $loader->load($fileFinder);
	}

	/**
	 * Returns the cache driver to use for caching the module routes
	 * @return CacheDriver the cache driver to use
	 */
	public function getCacheDriver()
	{
		if($this->cacheDriver === null){
			$this->setCacheDriver();
		}

		return $this->cacheDriver;
	}

	/**
	 * Sets the cachedriver.
	 * Default is Memcached.
	 */
	public function setCacheDriver()
	{
		if(class_exists('Memcached')){
			$memcached = new Memcached();
			$memcached->addServer("localhost", 11211);

			$this->cacheDriver = new MemcachedCache();
			$this->cacheDriver->setMemcached($memcached);	
		} else {
			$this->cacheDriver = new ArrayCache();
		}
		
	}

	/**
	 * Returns the module route directory
	 * @return string $directory the module directory
	 */
	private function getDirectory()
	{
		return $this->directory;
	}

	/**
	 * Sets the module route directory
	 * @param string $directory the module route directory
	 */
	private function setDirectory($directory)
	{
		$this->directory = $directory;
	}

	/**
	 * Returns the modules directory path, by using the get_template_directory() function
	 * @see https://codex.wordpress.org/Function_Reference/get_template_directory information about the function
	 * @return string the module directory path
	 */
	public function getModulesDirectory()
	{
		return get_template_directory().$this->defaultDirectory;
	}

	
	/**
	 * Returns the modules directory uri, by using the get_template_directory_uri() function
	 * @see https://codex.wordpress.org/Function_Reference/get_template_directory_uri information about the function
	 * @return string the module directory uri
	 */
	public function getModulesDirectoryUri()
	{
		return get_template_directory_uri().$this->defaultDirectory;
	}

}