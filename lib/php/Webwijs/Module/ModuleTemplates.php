<?php

namespace Webwijs\Module;

use Webwijs\Util\Strings;

/**
 * Module Templates
 *
 * Handling the modules templates to use with the Wordpress templating system
 *
 * @author Leo Flapper <leo@webwijs.nu>
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class ModuleTemplates
{

	/**
	 * Contains al modules it's templates
	 * @var array
	 */
	protected $modulesTemplates = array();

	/**
	 * Contains al modules it's single templates
	 * @var array
	 */
	protected $moduleSingleTemplates = array();

	/**
	 * Contains all modules
	 * @var array
	 */
	protected $modules = array();

    /**
     * Sets the modules
     * @param array $modules array containing the modules
     */
    public function __construct($modules) {
    	$this->setModules($modules);
    	
    	if($this->isWpDebug()){
    		$this->registerModulesTemplates();	
    	}

    }

    /**
     * Initiates the various filters to handle module templates
     * @return void
     */
    public function init()
    {
    	// Add a filter to the attributes metabox to inject template into the cache.
        add_filter('page_attributes_dropdown_pages_args', array($this, 'registerModulesTemplates'));

        // Add a filter to the save post to inject out template into the page cache
        add_filter('wp_insert_post_data', array($this, 'registerModulesTemplates'));

        // Before the page layout on post admin pages is displayed
        add_filter('before_page_layout_display', array($this, 'registerModulesTemplates' ));

        // Add a filter to load the module template if allowed an available
        add_filter('template_include', array( $this, 'viewModuleTemplate'));
    }

    /**
     * Checks if the template is a default single template, if so it will check if a module has the single template set.
     * If the template is not a single template, it will check whether the post has a module page template set.
     * @param string $template the current template which is used
     * @return string $the template to use
     */
    public function viewModuleTemplate( $template ) {
        global $post;

        // Only the default single template, if it's not the default it means that there is a single template available in the root of the theme folder
        if($this->isDefaultSingleTemplate($template)){
        	if($moduleSingleTemplates = $this->getModulesSingleTemplates()){
	    		if(!$baseName = $this->getSingleTemplatePostType($template)){
	    			if(isset($moduleSingleTemplates[$post->post_type])){
	    				$template = $moduleSingleTemplates[$post->post_type];
	    			}
	    		}
	        }
        } else {
			if($modulesTemplates = $this->getModulesTemplates()){
	        	if (isset($modulesTemplates[get_post_meta($post->ID, '_wp_page_template', true )])){
	                $file = get_post_meta($post->ID, '_wp_page_template', true);
		            if(file_exists($file)){
		                $template = $file;
		            }
	            } 
        	}
        }

        return $template;
    } 

    /**
     * Registers the modules templates for use within the Wordpress templating system
     * @param  array  $attributes optional attributes which are passed for declaring the templates
     * @return array  $attributes the attributes which are passed through
     */
    public function registerModulesTemplates($attributes = array())
    {
    	if($modulesTemplates = $this->getModulesTemplates()){
    		$cacheKey = 'page_templates-' . md5(get_theme_root() . '/' . get_stylesheet());
			
			$templates = wp_get_theme()->get_page_templates();
			
			if (empty($templates)) {
	            $templates = array();
	        } 

	        wp_cache_delete( $cacheKey, 'themes');

			$templates = array_merge($templates, $modulesTemplates);
	        
	        wp_cache_add( $cacheKey, $templates, 'themes', 1800 );
	        
    	}

    	return $attributes;
    }

    /**
     * Checks if the current template is the default single template from Wordpress (single.php in the root theme folder)
     * @param  string  $template the template file
     * @return boolean true if default single template, false if not
     */
    private function isDefaultSingleTemplate($template)
    {
    	if (!is_string($template)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects an string argument; received "%s"',
	            __METHOD__,
	            (is_object($template) ? get_class($template) : gettype($template))
	        ));
	    }

    	if(preg_match('/\/(single)\.[^.]+$/', $template, $matpathches)){
    		if(isset($matches[1])){
    			return true;
    		}
    	}
    	return false;
    }

    /**
     * Returns the modules templates
     * @return array $moduleTemplates associative array containing modules templates
     */
    public function getModulesTemplates()
    {
    	if(!$this->modulesTemplates){
    		$this->setModulesTemplates();
    	}

    	return $this->modulesTemplates;
    }

    /**
     * Returns the modules single templates
     * @return array $moduleSingleTemplates associative array containing modules single templates
     */
    public function getModulesSingleTemplates()
    {
    	if(!$this->moduleSingleTemplates){
    		$this->setModulesSingleTemplates();
    	}

    	return $this->moduleSingleTemplates;
    }

    /**
     * Sets the modules single templates by iterating through each module single templates and retrieving the template name and path
     * @return void
     */
    private function setModulesSingleTemplates()
    {
    	foreach($this->getModules() as $module){
    		if($module->isActive()){
	    		foreach($module->getSingleTemplates() as $info){	
					$postType = $this->getSingleTemplatePostType($info->getRealPath());
					$this->moduleSingleTemplates[$postType] = $info->getRealPath();
				}
    		}
		}		
    }

    /**
     * Sets the modules templates by iterating through each module templates and retrieving the template name and path.
     *
     * @link http://php.net/manual/ru/splfileinfo.getpathname.php#102987 inconsistencies with getPathname on Windows OS.
     */
    private function setModulesTemplates()
    {   
        // absolute path to current theme.
        $templateDir = realpath(get_template_directory());
    	foreach($this->getModules() as $module){
    		if($module->isActive()){
	    		foreach($module->getTemplates() as $info){
					if (!preg_match('/Template Name:(.*)$/mi', file_get_contents($info->getRealPath()), $header)) {
						continue;
                    }
                    
                    // obtain relative path from theme directory.
                    $path = $info->getRealPath();
                    if (Strings::startsWith($path, $templateDir)) {
                        $path = ltrim(substr($path, strlen($templateDir)), '\\');
                    }

					$name = sprintf('Module %s - %s', $module->getName(), _cleanup_header_comment($header[1]));
					$this->modulesTemplates[$path] = $name;
				}
    		}
		}
    }

    /**
     * Returns the post type of the single template if available.
     * 
     * Example:	single-employee.php is the single of the post type employee. 
     * 			So in this case it will return 'employee'
     * 			
     * @param  string $template the template to check
     * @return string $result the post type or an empty string if no post type
     */
    private function getSingleTemplatePostType($template)
    {
    	if (!is_string($template)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects an string argument; received "%s"',
	            __METHOD__,
	            (is_object($template) ? get_class($template) : gettype($template))
	        ));
	    }

    	$result = '';

    	if(preg_match('/\/single-(.*)\.[^.]+$/', $template, $matches)){
    		if(isset($matches[1])){
    			$result = $matches[1];	
    		}
    	}

    	return $result;
    }

    /**
     * Returns the modules
     * @return array $modules array containg the modules
     */
    private function getModules()
    {
    	return $this->modules;
    }

    /**
	 * Sets an array of modules
	 * @param array $modules the array containing the modules
	 * @return array $modules returns the modules container
	 */
	private function setModules($modules)
	{
		if (!is_array($modules) && !($modules instanceof \Traversable)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects an array or instance of the Traversable; received "%s"',
	            __METHOD__,
	            (is_object($modules) ? get_class($modules) : gettype($modules))
	        ));
	    }

	    foreach($modules as $name => $module){
	    	$lookup = strtolower($name);
    		$this->modules[$lookup] = $module;
	    }

	}

	/**
	 * Check if WP_DEBUG is set to true or false
	 * @return boolean true if on debug, false if not
	 */
	private function isWpDebug()
	{
		return WP_DEBUG;
	}
}
