<?php

namespace Webwijs\Module;

/**
 *
 * Module Aware Interface
 *
 * A module doesn't have to comply to the bootstrap rules defined in the Webwijs theme.
 * If the module contains this interface, it has to implement the following functions
 * 
 * @author Leo Flapper <leo@webwijs.nu>
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0.
 */
interface ModuleAwareInterface
{	

	/**
	 * This sets the module data for the module to use
	 * @param Module $module the module data class
	 */
	public function setModule(Module $module);

}