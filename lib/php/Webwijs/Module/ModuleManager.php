<?php

namespace Webwijs\Module;

use Webwijs\Module\Config\Loader as ConfigLoader;
use Webwijs\Module\Module;
use Webwijs\Module\ModuleTemplates;

/**
 * Module Manager
 *
 * Handling the modules with it's data
 *
 * @author Leo Flapper <leo@webwijs.nu>
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 */
class ModuleManager
{
	
	/**
	 * The module container
	 * @var array $modules, array containing modules
	 */
	protected static $modules = array();

	/**
	 * The modules directory path
	 * @var string $directory the modules directory path
	 */
	private $directory;

	/**
	 * The modules directory uri
	 * @var The modules directory uri
	 */
	private $directoryUri;

	/**
	 * Sets the modules, modules directory and module URI
	 * @param array  $modules       associative array containing modules
	 * @param string $directory     the modules directory path
	 * @param string $directoryUri  the modules directory uri
	 */
	public function __construct($modules, $directory, $directoryUri)
	{
		$this->setModules($modules);
		$this->setDirectory($directory);
		$this->setDirectoryUri($directoryUri);
	}

	/**
	 * Searches for modules and loads their config files.
	 * If the module is active, the bootstrap will be loaded
	 * @return array $modules array of modules and it's data
	 */
	public function initModules()
	{	
		$this->loadConfigs();
		$this->loadModules();
		$this->initTemplates();
			
		return self::getModules();
	}

	/**
	 * Loads the module config files
	 * @return array $configs array containing module configurations where the keys are the module names
	 */
	private function loadConfigs()
	{
		if ($modules = self::getModules()){
			$this->setConfigs(new ConfigLoader($modules, 'xml'));
		}
		return $this->getConfigs();
	}

	/**
	 * Sets the module data and bootstraps the module
	 * @return array $modules array containing modules where the keys are the module names
	 */
	private function loadModules()
	{
		foreach($this->getModules() as $name => $module){
			$this->setModule($name, new Module($module['name'], $module['path'], $this->getDirectoryUri().'/'.$name, $this->configs->getConfig($name)));
		}
		return $this->getModules();
	}

	/**
	 * Initiates the templates to use with the Wordpress templating system
	 * @return void
	 */
	private function initTemplates()
	{
		$templates = new ModuleTemplates($this->getModules());
		$templates->init();
	}

	/**
	 * Sets an array of modules
	 * @param array $modules the array containing the modules
	 * @return array $modules returns the modules container
	 */
	private function setModules($modules)
	{
		if (!is_array($modules) && !($modules instanceof \Traversable)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects an array or instance of the Traversable; received "%s"',
	            __METHOD__,
	            (is_object($modules) ? get_class($modules) : gettype($modules))
	        ));
	    }

	    foreach($modules as $name => $module){
	    	$this->setModule($name, $module);
	    }

	    return $this->getModules();
	}

	/**
	 * Sets a single module.
	 * The name will be used as key for the module.
	 * @param string $name the name of the module which will be used as key
	 * @param array $module a single module containing it's data
	 * @return array $module the module.
	 */
	private function setModule($name, $module)
	{
		if (!is_string($name)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects a string argument; received "%s"',
	            __METHOD__,
	            (is_object($name) ? get_class($name) : gettype($name))
	        ));
	    }

    	$lookup = strtolower($name);
    	   	
    	static::$modules[$lookup] = $module;
	}

	/**
	 * Returns a single module by name
	 * @param  string $name the name of the module
	 * @return Module|null the module if found, or null if not found
	 */
	private function getModule($name)
	{
		if (!is_string($name)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects a string argument; received "%s"',
	            __METHOD__,
	            (is_object($name) ? get_class($name) : gettype($name))
	        ));
	    }

    	$lookup  = strtolower($name);
    	$modules = self::getModules();
    	
		return (isset($modules[$lookup])) ? $modules[$lookup] : null;
	}

	/**
	 * Sets the module config data
	 * @param ConfigLoader $configs array containing config data objects
	 */
	private function setConfigs(ConfigLoader $configs)
	{
		$this->configs = $configs;
	}

	/**
	 * Returns the config data array
	 * @return ConfigLoader $configs array containing config data objects
	 */
	private function getConfigs()
	{
		return $this->configs;
	}

	
	/**
	 * Sets the modules directory path
	 * @param string $directory the modules directory path
	 */
	private function setDirectory($directory)
	{
		if (!is_string($directory)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects a string argument; received "%s"',
	            __METHOD__,
	            (is_object($directory) ? get_class($directory) : gettype($directory))
	        ));
	    }

		$this->directory = $directory;
	}

	/**
	 * Returns the modules directory path
	 * @return string $directory the module directory path
	 */
	private function getDirectory()
	{
		return $this->directory;
	}

	/**
	 * Sets the modules directory uri
	 * @param string $directoryUri the modules directory uri
	 */
	private function setDirectoryUri($directoryUri)
	{
		if (!is_string($directoryUri)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects a string argument; received "%s"',
	            __METHOD__,
	            (is_object($directoryUri) ? get_class($directoryUri) : gettype($directoryUri))
	        ));
	    }

		$this->directoryUri = $directoryUri;
	}

	/**
	 * Returns the modules directory uri
	 * @return string $directoryUri the module directory uri
	 */
	private function getDirectoryUri()
	{
		return $this->directoryUri;
	}
	
	/**
	 * Returns a collection of loaded modules.
	 
	 * @return array a collection of modules, or empty array if no modules have been loaded.
	 */
	public static function getModules()
	{
		return static::$modules;
	}
}
