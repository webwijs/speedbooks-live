<?php

namespace Webwijs;

use Webwijs\Autoloader as OldAutoloader;
use Webwijs\Loader\AutoloaderFactory;
use Webwijs\Loader\AutoloaderAdapter;
use Webwijs\Loader\ClassLoader;
use Webwijs\Loader\ClassLoaderAdapter;

use Webwijs\Module\ModuleFacade;
use Webwijs\Module\ModuleManager;

class Application
{
    public static $serviceManager;
    public static $modelManager;

    public function init()
    {
        foreach (get_class_methods($this) as $method) {
            if (strpos($method, '_init') === 0) {
                $this->$method();
            }
        }
    }

    protected function _initAutoload()
    {
        $autoloadFile = dirname(__DIR__) . '/vendor/autoload.php';
        if(!file_exists($autoloadFile)) {
            throw new \Exception(sprintf('Autoload file at path "%s" not found, did you run composer.json in the lib/php folder?', $autoloadFile));
        }
            
        require_once $autoloadFile;
    }

    protected function _initAutoloaderConfig()
    {
        require_once __DIR__ . '/Loader/AutoloaderFactory.php';
        
        AutoloaderFactory::factory(array(
            'Webwijs\Loader\StandardAutoloader' => array(
                'autoregister_webwijs' => true
            ),
        ));
    }
    
    /**
     * Add static resources to the class loader.
     */
    protected function _initResourceloaders()
    {    
        ClassLoader::addStaticResources(array(
            'viewhelper'        => 'Webwijs\View\Helper',
            'formdecorator'     => 'Webwijs\Form\Decorator',
            'formelement'       => 'Webwijs\Form\Element',
            'validator'         => 'Webwijs\Validate',
            'facetsearch'       => 'Webwijs\FacetSearch',
            'facetsearchfilter' => 'Webwijs\FacetSearch\Filter',
            'model'             => 'Webwijs\Model',
            'modeltable'        => 'Webwijs\Model\Table',
            'service'           => 'Webwijs\Service',
        ));
    }    
        
    protected function _initSetup()
    {
        add_action('after_switch_theme', array('Webwijs\Action\MySQL', 'setupRelatedPosts'));
    }
    
    protected function _initSession()
    {
        session_start();
    }
    
    protected function _initUrls()
    {
        if (get_option('theme_advanced_flat_url')) {
            add_action('post_type_link', array('Webwijs\Action\FlatUrl', 'createCustomPermalink'), 10, 100);
            add_action('page_link', array('Webwijs\Action\FlatUrl', 'createPagePermalink'), 10, 100);
            add_action('parse_request', array('Webwijs\Action\FlatUrl', 'parseRequest'));
        }
        else {
            add_filter('term_link', array('Webwijs\Action\NestedUrl', 'createTermLink'), 10, 3);
            add_action('parse_request', array('Webwijs\Action\NestedUrl', 'parseRequest'));
            add_filter('get_pagenum_link', array('Webwijs\Action\NestedUrl', 'pagenumLink'));
            add_filter('redirect_canonical', array('Webwijs\Action\NestedUrl', 'redirectCanonical'), 10, 2);
            add_filter('year_link', array('Webwijs\Action\NestedUrl', 'yearLink'), 10, 2);
            add_filter('month_link', array('Webwijs\Action\NestedUrl', 'monthLink'), 10, 3);
        }
    }

    protected function _initErrorPage()
    {
        add_action('wp', array('Webwijs\Action\NotFound', 'query404'), 10, 1);
        add_filter('redirect_canonical', array('Webwijs\Action\NotFound', 'stopRedirect'), 10, 2);
        add_filter('404_template', array('Webwijs\Action\NotFound', 'locateTemplate'), 10, 1);
    }
    
    protected function _initSeo()
    {
        add_filter('wpseo_sitemap_exclude_taxonomy', array('Webwijs\Filter\Seo', 'excludeTaxonomy'), 10, 2);
    }

    /**
     * Initiates the module manager for loading the modules
     */
    protected function _initModules()
    {
        $moduleFacade = new ModuleFacade();
        if($modules = $moduleFacade->getModules()){
            $moduleManager = new ModuleManager($modules, $moduleFacade->getModulesDirectory(), $moduleFacade->getModulesDirectoryUri());
            $moduleManager->initModules();
        }
    }

    protected function _initTemplateLoader()
    {
        add_action('template_redirect', array('Webwijs\Template\Loader', 'load'), 100);
    }
    
    protected function _initWpQuery()
    {
        add_filter('posts_clauses', array('Webwijs\Filter\CustomQuery', 'filter'), 10, 2);
    }

    /**
     * This method remains to support code that still relies on the now deprecated
     * {@link Webwijs\Autoloader} class.
     *
     * @param Webwijs\Autoloader autoloader to resolve class names to file paths.
     * @deprecated 1.1.0 use the autoloader factory instead.
     */
    public static function pushAutoloader($autoloader)
    {        
        if ($autoloader instanceof OldAutoloader) {
            // append prefixes to the StandardAutoloader.
            AutoloaderFactory::factory(array(
                'Webwijs\Loader\StandardAutoloader' => new AutoloaderAdapter($autoloader)
            ));
            
            // expand the stack of static resources.
            ClassLoader::addStaticResources(new ClassLoaderAdapter($autoloader));
        }
    }

    public static function getServiceManager()
    {
        if (is_null(self::$serviceManager)) {
            self::$serviceManager = new ServiceManager();
        }
        return self::$serviceManager;
    }
    public static function getModelManager()
    {
        if (is_null(self::$modelManager)) {
            self::$modelManager = new ModelManager();
        }
        return self::$modelManager;
    }
}
