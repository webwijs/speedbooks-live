jQuery(function($) {

	$('html').on('click', function(e) {
		// $('.search-form', '.header').removeClass('focus hover');
	});


	$('.toggle').on('click', function(e) {
		e.preventDefault();
		var target = $($(this).attr('href'));
		target.toggleClass('active');
		if ( $(this).hasClass('searchbtn') ){
			$('i',this).toggleClass('fa-times');
		}
		if ( $(this).hasClass('menubtn') ){
			$('.extrabuttons').toggle();
			if($('#search').hasClass('active')){
				$('#search').toggleClass("active");
				$('.searchbtn i').toggleClass('fa-times');
			}
			
		}
	});

	if($('.slick').length > 0){
		$('.slick').slick({
			infinite: true,
			speed: 1000,
			fade: true,
			autoplay: true,
  			autoplaySpeed: 5000,
  			arrows: false,
  			dots: true,
  			responsive: [
			{
				breakpoint: 768,
				settings: {
					autoplay: false
				}
			},
			{
				breakpoint: 480,
				settings: {
					autoplay: false
				}
			}]
	  	});
	}

	if ($('body').width() > 640) {
        var fromTop = $(document).scrollTop();
        $('.mainnav .bol').toggleClass("scrolled", (fromTop > 20));
    }

	$(window).on("scroll", function(event) {
        if ($('body').width() > 640) {
            var fromTop = $(document).scrollTop();
            $('.mainnav .bol').toggleClass("scrolled", (fromTop > 20));
        }
    });

	$('.masonry').masonry({

		itemSelector: '.masonry article'

	});

});
