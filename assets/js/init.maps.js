jQuery( document ).ready(function($) {
	function initialize() {
		var mapCanvas = document.getElementById('map-canvas');
		var address = $('#address').val();

		var geocoder = new google.maps.Geocoder();
	    geocoder.geocode({
	        'address': address
	    }, function(results, status) {

	    	console.log(results);
	        if (status == google.maps.GeocoderStatus.OK) {

	            var myOptions = {
	                zoom: 12,
	                center: new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()),
	                mapTypeId: google.maps.MapTypeId.ROADMAP,
	                disableDefaultUI: true,
	                scrollwheel: false,
	            }

	            map = new google.maps.Map(mapCanvas, myOptions);

				// map.set('styles', [
				//     {
				//         "featureType": "administrative",
				//         "elementType": "labels.text.fill",
				//         "stylers": [
				//             {
				//                 "color": "#444444"
				//             }
				//         ]
				//     },
				//     {
				//         "featureType": "landscape",
				//         "elementType": "all",
				//         "stylers": [
				//             {
				//                 "color": "#f2f2f2"
				//             }
				//         ]
				//     },
				//     {
				//         "featureType": "poi",
				//         "elementType": "all",
				//         "stylers": [
				//             {
				//                 "visibility": "off"
				//             }
				//         ]
				//     },
				//     {
				//         "featureType": "road",
				//         "elementType": "all",
				//         "stylers": [
				//             {
				//                 "saturation": -100
				//             },
				//             {
				//                 "lightness": 45
				//             }
				//         ]
				//     },
				//     {
				//         "featureType": "road.highway",
				//         "elementType": "all",
				//         "stylers": [
				//             {
				//                 "visibility": "simplified"
				//             }
				//         ]
				//     },
				//     {
				//         "featureType": "road.arterial",
				//         "elementType": "labels.icon",
				//         "stylers": [
				//             {
				//                 "visibility": "off"
				//             }
				//         ]
				//     },
				//     {
				//         "featureType": "transit",
				//         "elementType": "all",
				//         "stylers": [
				//             {
				//                 "visibility": "off"
				//             }
				//         ]
				//     },
				//     {
				//         "featureType": "water",
				//         "elementType": "all",
				//         "stylers": [
				//             {
				//                 "color": "#48bdec"
				//             },
				//             {
				//                 "visibility": "on"
				//             }
				//         ]
				//     }
				// ]);

	            var marker = new google.maps.Marker({
	                map: map,
	                position: results[0].geometry.location,
	                icon: $('#styles').val()+'/assets/img/marker.png'
	            });
	            marker.info = new google.maps.InfoWindow({
					content: '<div style="text-align:center;">'+address+'</div>'
				});

				google.maps.event.addListener(marker, 'click', function() {
					marker.info.open(map, marker);
				});
	        }
	    });

	}
	google.maps.event.addDomListener(window, 'load', initialize);
});
