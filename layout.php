<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<head>
	<title><?php wp_title('') ?></title>
	<meta charset="<?php bloginfo('charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/assets/img/favicon.ico" />
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	
	<script src="https://use.typekit.net/mef2iiv.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	
	<?php wp_enqueue_style('foundation', get_bloginfo('stylesheet_directory') . '/assets/lib/foundation6/css/foundation.css', 'main') ?>
	<?php wp_enqueue_style('flags', get_bloginfo('stylesheet_directory') . '/assets/lib/flag-icon-css-master/css/flag-icon.min.css', 'main') ?>
	<?php wp_enqueue_style('text', get_bloginfo('stylesheet_directory') . '/assets/css/text.css') ?>
	<?php wp_enqueue_style('forms', get_bloginfo('stylesheet_directory') . '/assets/css/forms.css') ?>
	<?php wp_enqueue_style('slick', get_bloginfo('stylesheet_directory') . '/assets/css/slick.css') ?>
	<?php wp_enqueue_style('main', get_bloginfo('stylesheet_directory') . '/assets/css/main.css') ?>
	<?php wp_enqueue_style('chris', get_bloginfo('stylesheet_directory') . '/assets/css/chris.css') ?>	
	<?php wp_enqueue_style('print', get_bloginfo('stylesheet_directory') . '/assets/css/print.css', array(), false, 'print') ?>

	<?php wp_enqueue_script('jquery') ?>
	<?php wp_enqueue_script('foundation', get_bloginfo('stylesheet_directory') . '/assets/lib/foundation6/js/foundation.min.js', array('jquery'), false, true) ?>
	<?php wp_enqueue_script('modernizr', get_bloginfo('stylesheet_directory') . '/assets/lib/js/modernizr.js') ?>
	<?php wp_enqueue_script('slick', get_bloginfo('stylesheet_directory') . '/assets/js/slick.min.js', array('jquery'), false, true) ?> 
	<?php wp_enqueue_script('main', get_bloginfo('stylesheet_directory') . '/assets/js/main.js', array('jquery'), false, true) ?>
	<?php wp_head() ?>
	<script src="https://npmcdn.com/masonry-layout@4.0/dist/masonry.pkgd.min.js"></script>

	<!-- Hotjar Tracking Code for http://www.speedbooks.nl -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:159660,hjsv:5};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');

	</script>
</head>

<body <?php body_class() ?> itemscope itemtype="http://schema.org/WebPage">
	<div class="body-container">
		<div class="header-container">
			<?php echo $this->partial('partials/layout/header.phtml') ?>
		</div>
		<div class="main-container <?php if(is_front_page()): ?>dots<?php endif; ?>" role="main">
			<?php echo $this->content ?>
		</div>
	</div>
	<div class="footer-container">
		<?php echo $this->partial('partials/layout/footer.phtml'); ?>
	</div>
	<?php wp_footer() ?>
	<script type='text/javascript'>
		(function(d,t) {
			_scoopi = { 'onload': function() { 
				this.trkDocumentLoad(); 
			}};
			// var loaded = jQuery('script[src="//api.salesfeed.com/v3/bootstrap.js?aid=9z1a4nyx4hst"]').length;
			// if(!loaded){
				var s = d.getElementsByTagName(t)[0], js = d.createElement(t); 
				js.async = 1;
				js.src = '//api.salesfeed.com/v3/bootstrap.js?aid=9z1a4nyx4hst';
				s.parentNode.insertBefore(js,s);
			// }
			

		})(document,'script');
	</script>
</body>
</html>
